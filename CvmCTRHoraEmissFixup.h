#ifndef CVMCTRFIXUP_H
#define CVMCTRFIXUP_H

#include "ParserFixup.h"

//fixes HORA_EMISS field that may be with strange bars and shit;

class CvmCTRHoraEmissFixup : public ParserFixup
{
public:
	CvmCTRHoraEmissFixup(ParserDefinitions *defs);
	bool fixup(QString &value);
};

#endif // CVMCTRFIXUP_H
