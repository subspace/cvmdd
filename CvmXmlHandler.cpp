#include "CvmXmlHandler.h"
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

CvmXmlHandler::CvmXmlHandler(QSqlDatabase &db, quint32 downloadID): db(db), downloadID(downloadID)
{
	initMappings();
}

CvmXmlHandler::~CvmXmlHandler()
{
	cleanupMappings();
}

void CvmXmlHandler::cleanupMappings()
{
	QList<XmlSqlTableMap *>::iterator tableItr = tableMap.begin();
	while (tableItr != tableMap.end()) {
		delete (*tableItr)->model;
		delete *tableItr;
		tableItr++;
	}

	QList<XmlSqlColumnMap *>::iterator itr = columnMap.begin();
	while (itr != columnMap.end()) {
		delete *itr;
		itr++;
	}

	QList<XmlSqlColumnToColumnMap *>::iterator itr2 = columnToColumnMaps.begin();
	while (itr2 != columnToColumnMaps.end()) {
		delete *itr2;
		itr2++;
	}
}

CvmXmlHandler::XmlSqlTableMap *CvmXmlHandler::addTableMapping(const QString &xmlTableName, const QString &sqlTableName, XmlSqlTableMap *parentTable)
{
	QList<XmlSqlTableMap *>::iterator itr = tableMap.begin();
	XmlSqlTableMap *map;
	while (itr != tableMap.end()) {
		map = static_cast<XmlSqlTableMap *>(*itr);
		if (map->xmlTableName == xmlTableName) {
			map->sqlTableName = sqlTableName;
			map->model->setTable(sqlTableName);
			map->parentTable = parentTable;
			return map;
		}
		itr++;
	}

	map = new XmlSqlTableMap;
	map->xmlTableName = xmlTableName;
	map->sqlTableName = sqlTableName;
	map->parentTable = parentTable;
	map->model = new QSqlTableModel(0, db);
	map->model->setEditStrategy(QSqlTableModel::OnManualSubmit);
	map->model->setTable(sqlTableName);

	tableMap.push_back(map);

	return map;
}

CvmXmlHandler::XmlSqlColumnMap *CvmXmlHandler::addColumnMapping(XmlSqlTableMap *table, const QString &xmlColumnName, const QString &sqlColumnName, ParserDefinitions::FieldType type, bool isPrimaryKey, XmlSqlColumnMap *linkedColumn)
{
	QList<XmlSqlColumnMap *>::iterator itr = columnMap.begin();
	XmlSqlColumnMap *map;
	while (itr != columnMap.end()) {
		map = static_cast<XmlSqlColumnMap *>(*itr);

		if (map->getTable() == table && map->xmlColumnName == xmlColumnName) {
			map->sqlColumnName = sqlColumnName;
			map->type = type;
			map->isPrimaryKey = isPrimaryKey;
			map->linkedColumn = linkedColumn;
			if (type == ParserDefinitions::DateTime || type == ParserDefinitions::SmallDateTime) //set default format
				map->dateTimeFormat = "yyyy-MM-ddThh:mm:ss";
			else if (type == ParserDefinitions::Time)
				map->dateTimeFormat = "hh:mm:ss";

			return map;
		}
		itr++;
	}

	map = new XmlSqlColumnMap;
	map->setTable(table);
	map->xmlColumnName = xmlColumnName;
	map->sqlColumnName = sqlColumnName;
	map->type = type;
	map->isPrimaryKey = isPrimaryKey;
	map->linkedColumn = linkedColumn;
	if (type == ParserDefinitions::DateTime || type == ParserDefinitions::SmallDateTime) //set default format
		map->dateTimeFormat = "yyyy-MM-ddThh:mm:ss";
	else if (type == ParserDefinitions::Time)
		map->dateTimeFormat = "hh:mm:ss";

	columnMap.push_back(map);

	return map;
}

bool CvmXmlHandler::initMappings()
{
	if (!db.isValid())
		return false;

	cleanupMappings();

	tableMap.clear();
	columnMap.clear();
	columnToColumnMaps.clear();

	return true;
}

CvmXmlHandler::XmlSqlTableMap *CvmXmlHandler::findTableMap(const QString &xmlTableName, XmlSqlTableMap *parentTable) const
{
	for (int i = 0; i < tableMap.size(); i++) {
		if (tableMap[i]->xmlTableName == xmlTableName && parentTable == tableMap[i]->parentTable)
			return tableMap[i];
	}
	return NULL;
}

CvmXmlHandler::XmlSqlColumnMap *CvmXmlHandler::findColumnMap(const XmlSqlTableMap *table, const QString &xmlColumnName) const
{
	for (int i = 0; i < columnMap.size(); i++) {
		XmlSqlColumnMap *map = columnMap.at(i);

		if (map->getTable() == table && map->xmlColumnName == xmlColumnName)
			return map;
	}
	return NULL;
}

bool CvmXmlHandler::startDocument()
{
//	modifiedModels.clear();
	currentTable = NULL;
	return true;
}

void CvmXmlHandler::dumpModel(QSqlTableModel *model) const
{
	for (int i = 0; i < model->rowCount(); i++) {
		for (int j = 0; j < model->columnCount(); j++) {
			qDebug() << "row" << i << "col" << j << model->data(model->index(i, j));
		}
	}
}

bool CvmXmlHandler::endDocument()
{
//	for (int i = 0; i < modifiedModels.size(); i++) {
//		if (!modifiedModels.at(i)->submitAll()) {
//			qDebug() << modifiedModels.at(i)->tableName() << modifiedModels.at(i)->lastError().text();
//			dumpModel(modifiedModels.at(i));
//			return false;
//		}
//	}
	return true;
}

bool CvmXmlHandler::startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts)
{
	Q_UNUSED(namespaceURI);
	Q_UNUSED(qName);
	Q_UNUSED(atts);

	XmlSqlTableMap *tableMap = findTableMap(localName, currentTable);
	if (tableMap) {
		if (!tableMap->sqlTableName.isEmpty())
			tableMap->record = tableMap->model->record();
		currentTable = tableMap;
		qDebug() << "entered table" << tableMap->xmlTableName;
		return true;
	}
	currentElementValue.clear();

	return true;
}

bool CvmXmlHandler::characters(const QString &ch)
{
	currentElementValue.append(ch);
	return true;
}

bool CvmXmlHandler::checkConstraint(XmlSqlTableMap *table) const
{
	QString filter;

	for (int i = 0; i < columnMap.size(); i++)
	{
		XmlSqlColumnMap *col = columnMap.at(i);
		if (col->table == table && col->isPrimaryKey && !col->sqlColumnName.isEmpty())
			filter += col->sqlColumnName + " = " + "'" + table->record.value(col->sqlColumnName).toString() + "' AND ";
	}

	if (filter.isEmpty())
		return true;
	filter.chop(5);
	qDebug() << filter;

	table->model->setFilter(filter);
	table->model->select();
	if (table->model->rowCount())
		return false;

	return true;
}

bool CvmXmlHandler::addColumnToColumnMapping(XmlSqlColumnMap *origin, XmlSqlColumnMap *dest)
{
	XmlSqlColumnToColumnMap *map = new XmlSqlColumnToColumnMap;

	map->origin = origin;
	map->dest = dest;

	columnToColumnMaps.push_back(map);

	return true;
}

//bool CvmXmlHandler::checkConstraint(XmlSqlTableMap *table) const
//{
//	QSqlQuery	sql(db);
//	QString		filter;
//	QList<XmlSqlColumnMap *> constraintColumns;
//	XmlSqlColumnMap *col;

//	for (int i = 0; i < columnMap.size(); i++)
//	{
//		col = columnMap.at(i);
//		if (col->table == table && col->isPrimaryKey)
//		{
//			constraintColumns.push_back(col);
//			filter += col->sqlColumnName + " = ? AND ";
//		}
//	}
//	if (filter.isEmpty())
//		return true; //no constraint can insert

//	filter.chop(5);

//	sql.prepare(filter);
//	for (int i = 0; i < constraintColumns.size(); i++) {
//		col = constraintColumns.at(i);
//		sql.bindValue(i, table->record.value(col->sqlColumnName));
//	}

//	if (!sql.exec())
//		return false;

//	if (!sql.first())
//		return true; //ok can insert

//	return false;
//}

void CvmXmlHandler::linkedColumnRecursiveSet(XmlSqlColumnMap *column)
{
	if (column->linkedColumn)
	{
		column->linkedColumn->table->record.setValue(column->linkedColumn->sqlColumnName, column->table->record.value(column->sqlColumnName));

		if (!column->linkedColumn->linkedColumn)
			return;

		linkedColumnRecursiveSet(column->linkedColumn);
	}
}

bool CvmXmlHandler::endElement(const QString &namespaceURI, const QString &localName, const QString &qName)
{
	Q_UNUSED(namespaceURI);
	Q_UNUSED(qName);

	if (!currentTable) //already got out of all tables
		return true;

	XmlSqlTableMap *tableMap = findTableMap(localName, currentTable->parentTable);
	if (tableMap) {
		//dummy table
		if (!tableMap->sqlTableName.isEmpty()) {
			currentTable = tableMap->parentTable;
			qDebug() << "exited table" << tableMap->xmlTableName;
			return true;
		}

		//set downloadID
		tableMap->record.setValue("downloadid", QVariant(downloadID));

		//update values in column to column maps first
		for (int i = 0; i < columnToColumnMaps.size(); i++) {
			XmlSqlColumnToColumnMap *map = columnToColumnMaps.at(i);
			for (int j = 0; j < columnMap.size(); j++) {
				XmlSqlColumnMap *col = columnMap.at(j);
				if (col->table == tableMap && col == map->dest) {
					col->table->record.setValue(col->sqlColumnName, map->origin->table->record.value(map->origin->sqlColumnName));
				}
			}
		}

		//can insert no constraint problems
		if (checkConstraint(tableMap)) {
			qDebug() << "inserting" << tableMap->sqlTableName;

			if (!tableMap->model->insertRecord(-1, currentTable->record)) {
				qDebug() << tableMap->model->lastError().text();
				return false;
			}
			if (!tableMap->model->submitAll()) {
				qDebug() << tableMap->model->lastError().text();
				return false;
			}

		} else {
			qDebug() << "updating" << tableMap->sqlTableName;

			//update fields values
			for (int i = 0; i < tableMap->record.count(); i++)
				tableMap->model->setData(tableMap->model->index(0, i), tableMap->record.value(i));

			if (!tableMap->model->submitAll()) {
				qDebug() << tableMap->model->lastError().text();
				return false;
			}
		}

		//set linked columns values
		for (int i = 0; i < columnMap.size(); i++) {
			XmlSqlColumnMap *col = columnMap.at(i);
			if (col->table == tableMap) {
				if (col->linkedColumn)
					col->linkedColumn->table->record.setValue(col->linkedColumn->sqlColumnName, col->table->record.value(col->sqlColumnName));
			}
		}

		currentTable = tableMap->parentTable;
		qDebug() << "exited table" << tableMap->xmlTableName;
		return true;
	}

	XmlSqlColumnMap *colMap = findColumnMap(currentTable, localName);
	if (!colMap) {
//		qDebug() << "not found" << currentTable->xmlTableName << localName; //ignored
		return true;
	}

	if (colMap->type == ParserDefinitions::None || colMap->sqlColumnName.isEmpty()) //ignore column
		return true;

	QDateTime		dateTime;
	QTime			time;
	QVariant		value;

	switch (colMap->type) {
	case ParserDefinitions::Text:
	case ParserDefinitions::NText:
	case ParserDefinitions::VarChar:
	case ParserDefinitions::Char:
	case ParserDefinitions::NVarChar:
	case ParserDefinitions::NChar:
		value = QVariant(currentElementValue);
		break;
	case ParserDefinitions::TinyInt:
	case ParserDefinitions::SmallInt:
	case ParserDefinitions::Int:
	case ParserDefinitions::BigInt:
		value = QVariant(currentElementValue.replace(" ", "").toLongLong());
		break;
	case ParserDefinitions::Real:
	case ParserDefinitions::Float:
	case ParserDefinitions::Money:
	case ParserDefinitions::Decimal:
	case ParserDefinitions::Numeric:
	case ParserDefinitions::SmallMoney:
		currentElementValue = currentElementValue.replace(" ", "");
		currentElementValue = currentElementValue.replace(",", ".");
		value = QVariant(currentElementValue.toDouble());
		break;
	case ParserDefinitions::Bit:
		currentElementValue = currentElementValue.replace("true", "1");
		currentElementValue = currentElementValue.replace("false", "0");
		value = QVariant(QVariant(currentElementValue.toInt()).toBool());
		break;
	case ParserDefinitions::DateTime:
	case ParserDefinitions::SmallDateTime:
		dateTime = QDateTime::fromString(currentElementValue, colMap->dateTimeFormat);
		if (!dateTime.isValid())
			value = QVariant(QDateTime());
		else
			value = QVariant(dateTime);
		break;
	case ParserDefinitions::Time:
		time = QTime::fromString(currentElementValue, colMap->dateTimeFormat);
		if (!time.isValid())
			value = QVariant(QTime());
		else
			value = QVariant(time);
		break;
	case ParserDefinitions::ByteArray:
		value = QVariant(QByteArray::fromBase64(currentElementValue.toAscii()));
		break;
	case ParserDefinitions::None:
		break;
	}
//	qDebug() << currentTable->xmlTableName << colMap->sqlColumnName << value;
	currentTable->record.setValue(colMap->sqlColumnName, value);

	return true;
}
