#ifndef CVMXMLHANDLERFORMDEMONSTRACAOFINANCEIRAITR_H
#define CVMXMLHANDLERFORMDEMONSTRACAOFINANCEIRAITR_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerFormDemonstracaoFinanceiraITR : public CvmXmlHandler
{
public:
	CvmXmlHandlerFormDemonstracaoFinanceiraITR(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERFORMDEMONSTRACAOFINANCEIRAITR_H
