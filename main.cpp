#include "CvmApplication.h"
#include <QDate>
#include <QDebug>
#include <QPlastiqueStyle>
#include "MainWindow.h"
#include <QSqlTableModel>

int main(int argc, char *argv[])
{
	CvmApplication	a(argc, argv);

	a.setStyle(new QPlastiqueStyle);

	if (!a.init())
		return true;

	MainWindow		m;

	m.setApplication(&a);
	m.show();

	return a.exec();
}
//http://cvmweb.cvm.gov.br/cvmweb/ASP/ListaAtivos.zip
