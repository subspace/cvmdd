#ifndef CVMPARSERTHREADSTARTER_H
#define CVMPARSERTHREADSTARTER_H

#include <QThread>
#include <QSqlDatabase>
#include <QMutex>

class CvmParser;
class QSemaphore;

class CvmParserThreadStarter : public QThread
{
	Q_OBJECT
public:
private:
	static QList<CvmParser *>	activeParsers;
	static QMutex				activeParsersMutex;

public:
	quint32			getDownloadID() const { return this->downloadID; }
	const QString	&getFileName() const { return this->fileName; }
	static int		getActiveParserCount() { QMutexLocker locker(&activeParsersMutex); return activeParsers.size(); }

private:
	CvmParser			*parser;
	QSqlDatabase		db;
	QString				fileName;
	quint32				downloadID;
	QSemaphore			*pool;

	bool		openDB();

private slots:
	void		threadTerminated();

protected:
	void		run();

public:
	explicit	CvmParserThreadStarter(QSemaphore *pool, const QString &fileName, quint32 downloadID, QObject *parent = 0);
				~CvmParserThreadStarter();

public:
signals:
	void		parsingFinished(CvmParser *parser, bool success);
	void		parsingTerminated(CvmParser *parser);
};

#endif // CVMPARSERTHREADSTARTER_H
