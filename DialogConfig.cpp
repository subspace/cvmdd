#include "DialogConfig.h"
#include "ui_DialogConfig.h"
#include <QSettings>
#include <QFileDialog>

DialogConfig::DialogConfig(QWidget *parent) :
QDialog(parent),
ui(new Ui::DialogConfig)
{
	ui->setupUi(this);

	QSettings set;

	ui->cvmUsername->setText(set.value("cvm_username").toString());
	ui->cvmPassword->setText(set.value("cvm_password").toString());

	ui->dbType->setText(set.value("db_type").toString());
	ui->dbAddress->setText(set.value("db_address").toString());
	ui->dbPort->setText(set.value("db_port").toString());
	ui->dbName->setText(set.value("db_name").toString());
	ui->dbUsername->setText(set.value("db_username").toString());
	ui->dbPassword->setText(set.value("db_password").toString());
	ui->dbMaxConnections->setValue(set.value("db_max_connections").toInt());

	ui->dataDirectory->setText(set.value("data_directory").toString());
}

void DialogConfig::accept()
{
	QSettings set;

	set.setValue("cvm_username", ui->cvmUsername->text());
	set.setValue("cvm_password", ui->cvmPassword->text());

	set.setValue("db_type", ui->dbType->text());
	set.setValue("db_address", ui->dbAddress->text());
	set.setValue("db_port", ui->dbPort->text().toUInt());
	set.setValue("db_name", ui->dbName->text());
	set.setValue("db_username", ui->dbUsername->text());
	set.setValue("db_password", ui->dbPassword->text());
	set.setValue("db_max_connections", ui->dbMaxConnections->text().toUInt());

	set.setValue("data_directory", ui->dataDirectory->text());

	QDialog::accept();
}

void DialogConfig::on_toolButton_clicked()
{
	QString dir;

	dir = QFileDialog::getExistingDirectory(this, "Select Data Files Directory...");
	if (dir.isEmpty())
		return;
	ui->dataDirectory->setText(dir);
}

DialogConfig::~DialogConfig()
{
	delete ui;
}
