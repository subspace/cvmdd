#include "ParserDefinitions.h"

ParserDefinitions::~ParserDefinitions()
{
	for (int i = 0; i < fields.size(); i++) {
		delete fields.at(i);
	}
}

ParserDefinitions::ParserField *ParserDefinitions::findField(const QString &name) const
{
	for (int i = 0; i < fields.size(); i++) {
		if (fields.at(i)->name == name) {
			return fields.at(i);
		}
	}
	return NULL;
}

bool ParserDefinitions::setFormat(int field, const QString &dateFormat)
{
	if (fields[field]->type == ParserDefinitions::DateTime || fields[field]->type == ParserDefinitions::SmallDateTime) {
		fields[field]->format = dateFormat;
		return true;
	}
	return false;
}

bool ParserDefinitions::addField(const QString &name, quint32 size, FieldType type, ParserFixup *fix)
{
	ParserField *field;

	field = findField(name);
	if (!field)
		field = new ParserField();
	field->name = name;
	field->size = size;
	field->type = type;
	if (field->type == ParserDefinitions::DateTime || field->type == ParserDefinitions::SmallDateTime)
		field->format = "dd/MM/yyyy hh:mm:ss";
	field->fix = fix;

	fields.push_back(field);

	return true;
}

bool ParserDefinitions::removeField(const QString &name)
{
	ParserField *field;

	field = findField(name);
	if (!field)
		return false;
	fields.removeAll(field);
	delete field;

	return true;
}
