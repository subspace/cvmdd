#include "CvmParser.h"
#include "Parser.h"
#include "StaticParserDefinitions.h"
#include <QBuffer>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlError>
#include <quazip/quazip.h>
#include <quazip/quazipfile.h>
#include <QXmlSimpleReader>
#include <QXmlInputSource>
#include <QDebug>

#include "CvmXmlHandlerDocumento.h"
#include "CvmXmlHandlerAnexoDocumento.h"
#include "CvmXmlHandlerAnexoTexto.h"
#include "CvmXmlHandlerCompCapitSocDemonsFinanNegocios.h"
#include "CvmXmlHandlerFormDemonstracaoFinanceiraITR.h"
#include "CvmXmlHandlerInfoFinaDfin.h"
#include "CvmXmlHandlerInformFinanDemonstracaoFinan.h"
#include "CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg.h"
#include "CvmXmlHandlerPeriodoDemonstracaoFinanceira.h"
#include "CvmXmlHandlerResumInfoFinanDemoFinan.h"

CvmParser::CvmParser(QByteArray &fileData, QSqlDatabase &db, quint32 downloadID):
dbConnection(db), downloadID(downloadID)
{
	this->fileData = new QByteArray();
	fileBuffer = new QBuffer(this->fileData);
	*this->fileData = fileData;
	tableModel = new QSqlTableModel(0, db);
	filePath.clear();
}

CvmParser::CvmParser(const QString &fileName, QSqlDatabase &db, quint32 downloadID):
dbConnection(db), downloadID(downloadID)
{
	QFile f(fileName);
	f.open(QFile::ReadOnly);
	filePath = fileName;
	fileData = new QByteArray();
	fileBuffer = new QBuffer(fileData);

	*fileData = f.readAll();

	f.close();

	tableModel = new QSqlTableModel(0, db);
}

int CvmParser::getFileVersion(QuaZip &zip)
{
	if (zip.setCurrentFile("CONFIG.001", QuaZip::csInsensitive))
		return getOldFileFormatVersion(zip);
	return getNewFileFormatVersion(zip);
}

bool CvmParser::setParserAsActive(bool active)
{
	QSqlQuery sql(dbConnection);
	if (active) {
		sql.prepare("INSERT INTO parseractualdownload(downloadid) VALUES (?)");
		sql.bindValue(0, downloadID);
		if (!sql.exec())
			return false;
		sql.finish();
	} else {
		sql.prepare("DELETE FROM parseractualdownload WHERE downloadid = ?");
		sql.bindValue(0, downloadID);
		if (!sql.exec())
			return false;
		sql.finish();
	}
	return true;
}

bool CvmParser::parseOldVersion(QuaZip &zip, int version)
{
	QuaZipFile			f(&zip);
	ParserDefinitions	*pd;
	Parser				p;

	bool r = zip.goToFirstFile();
	while (r) {
		f.open(QuaZipFile::ReadOnly);

		QString fileName = zip.getCurrentFileName();

		if (!StaticParserDefinitions::fileNameExists(fileName)) {
			r = zip.goToNextFile();
			f.close();
			continue;
		}

		pd = StaticParserDefinitions::getParserDefinition(fileName, version);
		p.setDefinitions(pd);
		p.setData(QString(f.readAll().data()));
		if (!p.parse()) {
			cleanupSQLDatabase();
			return false;
		}
		if (!updateSQLDatabase(&p)) {
			cleanupSQLDatabase();
			return false;
		}

		f.close();

		r = zip.goToNextFile();
	}
	return true;
}

QuaZipFile *CvmParser::getMainFile(QuaZip *zip, int version) const
{
	QString		mainFileName;

	bool r = zip->goToFirstFile();
	while (r) {
		mainFileName = zip->getCurrentFileName();
		if ((version == CVM_VERSION_ENET_ITR && mainFileName.endsWith(".itr", Qt::CaseInsensitive)) || (version == CVM_VERSION_ENET_DFP && mainFileName.endsWith(".dfp", Qt::CaseInsensitive))) {
			QuaZipFile *file = new QuaZipFile(zip);
//			file->open(QuaZipFile::ReadOnly);
			return file;
		}
		r = zip->goToNextFile();
	}

	return NULL;
}

bool CvmParser::parseNewVersion(QuaZip &zip, int version)
{
	QuaZipFile *mainFile = getMainFile(&zip, version);

	if (!mainFile)
		return false;
	mainFile->open(QuaZipFile::ReadOnly);

	QBuffer		fileData;
	fileData.setData(mainFile->readAll());
	QuaZip		mainZip(&fileData);
	QuaZipFile	file(&mainZip);

	if (!mainZip.open(QuaZip::mdUnzip))
		return false;

	bool r = mainZip.goToFirstFile();
	while (r) {
		QString fileName = mainZip.getCurrentFileName();

		if (!StaticParserDefinitions::newVersionFileList.contains(fileName)) {
			qDebug() << "Unknown file" << fileName;
			r = mainZip.goToNextFile();
			continue;
		}

		CvmXmlHandler		*handler;

		handler = NULL;

		if (!fileName.compare("Documento.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerDocumento(dbConnection, downloadID);
		} else if (!fileName.compare("AnexoDocumento.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerAnexoDocumento(dbConnection, downloadID);
		} else if (!fileName.compare("AnexoTexto.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerAnexoTexto(dbConnection, downloadID);
		} else if (!fileName.compare("ComposicaoCapitalSocialDemonstracaoFinanceiraNegocios.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerCompCapitSocDemonsFinanNegocios(dbConnection, downloadID);
		} else if (!fileName.compare("FormularioDemonstracaoFinanceiraITR.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerFormDemonstracaoFinanceiraITR(dbConnection, downloadID);
		} else if (!fileName.compare("FormularioDemonstracaoFinanceiraDFP.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerFormDemonstracaoFinanceiraITR(dbConnection, downloadID);
		} else if (!fileName.compare("InfoFinaDFin.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerInfoFinaDfin(dbConnection, downloadID);
		} else if (!fileName.compare("InformacaoFinanceiraDemonstracaoFinanceira.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerInformFinanDemonstracaoFinan(dbConnection, downloadID);
		} else if (!fileName.compare("PagamentoProventoDinheiroDemonstracaoFinanceiraNegocios.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg(dbConnection, downloadID);
		} else if (!fileName.compare("PeriodoDemonstracaoFinanceira.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerPeriodoDemonstracaoFinanceira(dbConnection, downloadID);
		} else if (!fileName.compare("ResumoInformacaoFinanceiraDemonstracaoFinanceira.xml", Qt::CaseInsensitive)) {
			handler = new CvmXmlHandlerResumInfoFinanDemoFinan(dbConnection, downloadID);
		}

		if (!handler) {
			qDebug() << "No handler for file" << fileName;
			r = mainZip.goToNextFile();
			continue;
		}

		QBuffer				fileBuffer;
		if (!file.open(QuaZipFile::ReadOnly))
			return false;
		fileBuffer.setData(file.readAll());
		QXmlInputSource		inputSource(&fileBuffer);
		QXmlSimpleReader	reader;

		reader.setContentHandler(handler);
		if (!reader.parse(inputSource))
			return false;

		file.close();
		delete handler;

		r = mainZip.goToNextFile();
	}
	return true;
}

bool CvmParser::parse()
{
	QuaZip zip(fileBuffer);

	if (!zip.open(QuaZip::mdUnzip))
		return false;

	int version;
	version = getFileVersion(zip);
	if (version == CVM_VERSION_UNKNOWN)
		return false;

	if (!setParserAsActive(true))
		return false;

	if (version == CVM_VERSION_ENET_DFP || version == CVM_VERSION_ENET_ITR) {
		if (!parseNewVersion(zip, version))
			return false;
	} else {
		if (!parseOldVersion(zip, version))
			return false;
	}

	if (!setParserAsActive(false))
		return false;

	return true;
}

int CvmParser::getNewFileFormatVersion(QuaZip &zip)
{
	bool r = zip.goToFirstFile();
	while (r) {
		QString fileName = zip.getCurrentFileName();
		if (fileName.endsWith(".itr", Qt::CaseInsensitive))
			return CVM_VERSION_ENET_ITR;
		if (fileName.endsWith(".dfp", Qt::CaseInsensitive))
			return CVM_VERSION_ENET_DFP;
		r = zip.goToNextFile();
	}
	return CVM_VERSION_UNKNOWN;
}

int CvmParser::getOldFileFormatVersion(QuaZip &zip)
{
	ParserDefinitions *pd = StaticParserDefinitions::getParserDefinition("CONFIG.001");
	QuaZipFile f(&zip);
	f.open(QIODevice::ReadOnly);
	QString fileData(f.readAll().data());
	if (fileData.isEmpty())
		return CVM_VERSION_UNKNOWN;
	f.close();

	Parser p(fileData, pd);
	if (!p.parse())
		return CVM_VERSION_UNKNOWN;

	float fileVersion = p.value(0, "VER_PRG").toFloat();
	int version = 0;
	switch ((int)fileVersion) {
	case 1:
	case 2:
		version = CVM_VERSION_2;
		break;
	case 3:
		version = CVM_VERSION_3;
		break;
	case 4:
		version = CVM_VERSION_4;
		break;
	case 5:
		version = CVM_VERSION_5;
		break;
	case 6:
		version = CVM_VERSION_6;
		break;
	case 7:
		version = CVM_VERSION_7;
		break;
	case 8:
		version = CVM_VERSION_8;
		break;
	case 9:
		version = CVM_VERSION_9;
		if (fileVersion == 9.1f)
			version = CVM_VERSION_91;
		else if (fileVersion == 9.2f)
			version = CVM_VERSION_92;
		break;
	}
	return version;
}

bool CvmParser::updateSQLDatabase(const Parser *parser)
{
	QString tableName = parser->name();

	tableName.chop(4);
	tableModel->setTable(tableName);
	tableModel->setFilter("0=1");

	for (int i = 0; i < parser->size(); i++) {
		QSqlRecord rec = tableModel->record();

		for (int j = 0; j < parser->fieldCount(); j++)
			rec.setValue(parser->fieldName(j), parser->value(i, j));
		rec.setValue("downloadid", downloadID);

		if (!tableModel->insertRecord(-1, rec)) {
			qDebug() << tableName << tableModel->lastError().databaseText();
			return false;
		}
	}
	return true;
}

bool CvmParser::cleanupSQLDatabase()
{
	return cleanupSQLDatabase(dbConnection, downloadID);
}

bool CvmParser::cleanupSQLDatabase(QSqlDatabase &db, quint32 downloadID)
{
	QSqlQuery sql(db);

	for (int i = 0; i < StaticParserDefinitions::fileList.size(); i++) {
		QString tableName = StaticParserDefinitions::fileList.at(i);
		tableName.chop(4);

		sql.prepare("DELETE FROM " + tableName + " WHERE downloadid = ?");
		sql.bindValue(0, downloadID);
		if (!sql.exec())
			return false;
		sql.finish();
	}

	sql.prepare("DELETE FROM parseractualdownload WHERE downloadid = ?");
	sql.bindValue(0, downloadID);
	if (!sql.exec())
		return false;

	return true;
}

CvmParser::~CvmParser()
{
	delete fileBuffer;
	delete fileData;
	delete tableModel;
	dbConnection.close();
}
