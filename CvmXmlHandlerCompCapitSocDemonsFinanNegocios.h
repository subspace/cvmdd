#ifndef CVMXMLHANDLERCOMPCAPITSOCDEMONSFINANNEGOCIOS_H
#define CVMXMLHANDLERCOMPCAPITSOCDEMONSFINANNEGOCIOS_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerCompCapitSocDemonsFinanNegocios : public CvmXmlHandler
{
public:
	CvmXmlHandlerCompCapitSocDemonsFinanNegocios(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERCOMPCAPITSOCDEMONSFINANNEGOCIOS_H
