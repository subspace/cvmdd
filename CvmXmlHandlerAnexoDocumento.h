#ifndef CVMXMLHANDLERANEXODOCUMENTO_H
#define CVMXMLHANDLERANEXODOCUMENTO_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerAnexoDocumento : public CvmXmlHandler
{
public:
	CvmXmlHandlerAnexoDocumento(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERANEXODOCUMENTO_H
