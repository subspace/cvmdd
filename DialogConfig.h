#ifndef DIALOGCONFIG_H
#define DIALOGCONFIG_H

#include <QDialog>

namespace Ui {
	class DialogConfig;
}

class DialogConfig : public QDialog
{
	Q_OBJECT
	
public:
	explicit DialogConfig(QWidget *parent = 0);
	~DialogConfig();

public slots:
	void accept();
	
private:
	Ui::DialogConfig *ui;

private slots:
	void on_toolButton_clicked();

};

#endif // DIALOGCONFIG_H
