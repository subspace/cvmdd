#ifndef CVMXMLHANDLERRESUMINFOFINANDEMOFINAN_H
#define CVMXMLHANDLERRESUMINFOFINANDEMOFINAN_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerResumInfoFinanDemoFinan : public CvmXmlHandler
{
public:
	CvmXmlHandlerResumInfoFinanDemoFinan(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERRESUMINFOFINANDEMOFINAN_H
