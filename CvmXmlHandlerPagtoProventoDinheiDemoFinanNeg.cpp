#include "CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg.h"

CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg::CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *mainTable = addTableMapping("PagamentoProventoDinheiroDemonstracaoFinanceira", "tfrepgto_prov_dinh_dfin");
		XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin", mainTable);
			XmlSqlTableMap *documento = addTableMapping("Documento",	"tfredocm", form);
			addColumnMapping(documento,	"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
			addColumnMapping(form,		"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
			addColumnMapping(mainTable,	"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int))); //

		addColumnMapping(mainTable, "NumeroIdentificacaoPagamento", "num_idt_pgto_prov_dinh_dfin", ParserDefinitions::Int, true); //
		XmlSqlTableMap *event = addTableMapping("EventoOrigemProvento", "tfredet_domi", mainTable);
			XmlSqlTableMap *idioma = addTableMapping("Idioma", "tfreidio", event);
			addColumnMapping(idioma, "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, false,
			addColumnMapping(event,	 "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true)); //
			addColumnMapping(idioma, "NomeIdioma", "nome_idio", ParserDefinitions::VarChar); //
			addColumnMapping(idioma, "CodigoIdiomaSistemaOperacional", "cod_idio_so", ParserDefinitions::VarChar, true); //
			XmlSqlTableMap *dominio = addTableMapping("Dominio", "tfredomi", event);
				XmlSqlTableMap *versDominio = addTableMapping("VersaoDominio", "tfrevers_tab_domi", dominio);
					XmlSqlTableMap *tabDominio = addTableMapping("TabelaDominio", "tfretab_domi", versDominio);
					addColumnMapping(tabDominio, "NomeTabelaDominio", "nome_tab_domi", ParserDefinitions::VarChar); //
					addColumnMapping(tabDominio, "DescricaoTabelaDominio", "desc_tab_domi", ParserDefinitions::VarChar); //
					addColumnMapping(tabDominio,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(versDominio,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(dominio,		"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(event,			"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true)))); //
					addColumnMapping(tabDominio, "IndicadorPermiteAlteracao", "ind_perm_alte", ParserDefinitions::Bit); //
				addColumnMapping(versDominio,	"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(dominio,		"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(event,			"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true))); //
				addColumnMapping(versDominio, "IndicadorVersaoAtiva", "ind_vers_ativa", ParserDefinitions::Bit); //
			addColumnMapping(dominio, "CodigoTipoUtilizadorDominio", "cod_tipo_util_domi", ParserDefinitions::SmallInt); //
			addColumnMapping(dominio,	"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true,
			addColumnMapping(event,		"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true)); //
		addColumnMapping(event, "SiglaOpcaoDominio", "sigl_opc_domi", ParserDefinitions::VarChar); //
		addColumnMapping(event, "DescricaoOpcaoDominio", "desc_opc_domi", ParserDefinitions::VarChar); //
		addColumnMapping(event, "DescricaoPluralOpcaoDominio", "desc_plur_opc_domi", ParserDefinitions::VarChar); //

	addColumnMapping(mainTable, "DataAprovacaoProvento", "data_apro_prov", ParserDefinitions::DateTime); //

		XmlSqlTableMap *tipoProv = addTableMapping("TipoProvento", "tfredet_domi", mainTable);
			XmlSqlTableMap *idiomaTipoProv = addTableMapping("Idioma", "tfreidio", tipoProv);
			addColumnMapping(idiomaTipoProv, "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, false,
			addColumnMapping(tipoProv,	 "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true)); //
			addColumnMapping(idiomaTipoProv, "NomeIdioma", "nome_idio", ParserDefinitions::VarChar);//
			addColumnMapping(idiomaTipoProv, "CodigoIdiomaSistemaOperacional", "cod_idio_so", ParserDefinitions::VarChar, true);//
			XmlSqlTableMap *dominioTipoProv = addTableMapping("Dominio", "tfredomi", tipoProv);
				XmlSqlTableMap *versDominioTipoProv = addTableMapping("VersaoDominio", "tfrevers_tab_domi", dominioTipoProv);
					XmlSqlTableMap *tabDominioTipoProv = addTableMapping("TabelaDominio", "tfretab_domi", versDominioTipoProv);
					addColumnMapping(tabDominioTipoProv, "NomeTabelaDominio", "nome_tab_domi", ParserDefinitions::VarChar);//
					addColumnMapping(tabDominioTipoProv, "DescricaoTabelaDominio", "desc_tab_domi", ParserDefinitions::VarChar);//
					addColumnMapping(tabDominioTipoProv,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(versDominioTipoProv,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(dominioTipoProv,		"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(tipoProv,			"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true))));//
					addColumnMapping(tabDominioTipoProv, "IndicadorPermiteAlteracao", "ind_perm_alte", ParserDefinitions::Bit);//
				addColumnMapping(versDominioTipoProv,	"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(dominioTipoProv,		"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(tipoProv,			"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true)));//
				addColumnMapping(versDominioTipoProv, "IndicadorVersaoAtiva", "ind_vers_ativa", ParserDefinitions::Bit);//
			addColumnMapping(dominioTipoProv, "CodigoTipoUtilizadorDominio", "cod_tipo_util_domi", ParserDefinitions::SmallInt);//
			addColumnMapping(dominioTipoProv,	"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true,
			addColumnMapping(tipoProv,		"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true));//
		addColumnMapping(tipoProv, "SiglaOpcaoDominio", "sigl_opc_domi", ParserDefinitions::VarChar);//
		addColumnMapping(tipoProv, "DescricaoOpcaoDominio", "desc_opc_domi", ParserDefinitions::VarChar);//
		addColumnMapping(tipoProv, "DescricaoPluralOpcaoDominio", "desc_plur_opc_domi", ParserDefinitions::VarChar);//

	addColumnMapping(mainTable, "DataInicioPagamento", "data_inic_pgto", ParserDefinitions::DateTime);//

		XmlSqlTableMap *codEspecieAcao = addTableMapping("CodigoEspecieAcao", "tfredet_domi", mainTable);
			XmlSqlTableMap *idioEA = addTableMapping("Idioma", "tfreidio", codEspecieAcao);
			addColumnMapping(idioEA, "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, false,
			addColumnMapping(codEspecieAcao,	 "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true));//
			addColumnMapping(idioEA, "NomeIdioma", "nome_idio", ParserDefinitions::VarChar);//
			addColumnMapping(idioEA, "CodigoIdiomaSistemaOperacional", "cod_idio_so", ParserDefinitions::VarChar, true);//
			XmlSqlTableMap *dominioEA = addTableMapping("Dominio", "tfredomi", codEspecieAcao);
				XmlSqlTableMap *versDominioEA = addTableMapping("VersaoDominio", "tfrevers_tab_domi", dominioEA);
					XmlSqlTableMap *tabDominioEA = addTableMapping("TabelaDominio", "tfretab_domi", versDominioEA);
					addColumnMapping(tabDominioEA, "NomeTabelaDominio", "nome_tab_domi", ParserDefinitions::VarChar);//
					addColumnMapping(tabDominioEA, "DescricaoTabelaDominio", "desc_tab_domi", ParserDefinitions::VarChar);//
					addColumnMapping(tabDominioEA,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(versDominioEA,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(dominioEA,		"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(codEspecieAcao,			"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true))));//
					addColumnMapping(tabDominioEA, "IndicadorPermiteAlteracao", "ind_perm_alte", ParserDefinitions::Bit);//
				addColumnMapping(versDominioEA,	"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(dominioEA,		"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(codEspecieAcao,			"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true)));//
				addColumnMapping(versDominioEA, "IndicadorVersaoAtiva", "ind_vers_ativa", ParserDefinitions::Bit);//
			addColumnMapping(dominioEA, "CodigoTipoUtilizadorDominio", "cod_tipo_util_domi", ParserDefinitions::SmallInt);//
			addColumnMapping(dominioEA,	"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true,
			addColumnMapping(codEspecieAcao,		"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true));//
			XmlSqlTableMap *dominioPai = addTableMapping("DominioPai", "tfredomi", dominioEA);
				XmlSqlTableMap *verDomPai = addTableMapping("VersaoDominio", "tfrevers_tab_domi", dominioPai);
					XmlSqlTableMap *tabDomPai = addTableMapping("TabelaDominio", "tfretab_domi", verDomPai);
					addColumnMapping(tabDomPai,		"CodigoTabelaDominio", "cod_tab_domi",		ParserDefinitions::SmallInt, true,
					addColumnMapping(verDomPai,		"CodigoTabelaDominio", "cod_tab_domi",		ParserDefinitions::SmallInt, true,
					addColumnMapping(dominioPai,	"CodigoTabelaDominio", "cod_tab_domi",		ParserDefinitions::SmallInt, true,
					addColumnMapping(dominioEA,		"CodigoTabelaDominio", "cod_tab_domi_relc",	ParserDefinitions::SmallInt, true))));//
				addColumnMapping(verDomPai,		"NumeroVersaoDominio", "num_vers_domi",			ParserDefinitions::SmallInt, true,
				addColumnMapping(dominioPai,	"NumeroVersaoDominio", "num_vers_domi",			ParserDefinitions::SmallInt, true,
				addColumnMapping(dominioEA,		"NumeroVersaoDominio", "num_vers_domi_relc",	ParserDefinitions::SmallInt, true)));//
			addColumnMapping(dominioPai,	"CodigoOpcao", "cod_opc_domi",		ParserDefinitions::SmallInt, true,
			addColumnMapping(dominioEA,		"CodigoOpcao", "cod_opc_domi_relc", ParserDefinitions::SmallInt, true));//
		addColumnMapping(codEspecieAcao, "SiglaOpcaoDominio", "sigl_opc_domi", ParserDefinitions::VarChar);//
		addColumnMapping(codEspecieAcao, "DescricaoOpcaoDominio", "desc_opc_domi", ParserDefinitions::VarChar);//
		addColumnMapping(codEspecieAcao, "DescricaoPluralOpcaoDominio", "desc_plur_opc_domi", ParserDefinitions::VarChar);//

		XmlSqlTableMap *codClasseAcaoPref = addTableMapping("TipoProvento", "tfredet_domi", mainTable);
			XmlSqlTableMap *idiomaAcaoPref = addTableMapping("Idioma", "tfreidio", codClasseAcaoPref);
			addColumnMapping(idiomaAcaoPref, "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, false,
			addColumnMapping(codClasseAcaoPref,	 "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true));//whut?
			addColumnMapping(idiomaAcaoPref, "NomeIdioma", "nome_idio", ParserDefinitions::VarChar); //empty
			addColumnMapping(idiomaAcaoPref, "CodigoIdiomaSistemaOperacional", "cod_idio_so", ParserDefinitions::VarChar, true); //empty
			XmlSqlTableMap *dominioAcaoPref = addTableMapping("Dominio", "tfredomi", codClasseAcaoPref);
				XmlSqlTableMap *versDomiAcaoPref = addTableMapping("VersaoDominio", "tfrevers_tab_domi", dominioAcaoPref);
					XmlSqlTableMap *tabDomiAcaoPref = addTableMapping("TabelaDominio", "tfretab_domi", versDomiAcaoPref);
					addColumnMapping(tabDomiAcaoPref, "NomeTabelaDominio", "nome_tab_domi", ParserDefinitions::VarChar); //empty
					addColumnMapping(tabDomiAcaoPref, "DescricaoTabelaDominio", "desc_tab_domi", ParserDefinitions::VarChar); //empty
					addColumnMapping(tabDomiAcaoPref,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(versDomiAcaoPref,	"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(dominioAcaoPref,		"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true,
					addColumnMapping(codClasseAcaoPref,			"CodigoTabelaDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true)))); //empty
					addColumnMapping(tabDomiAcaoPref, "IndicadorPermiteAlteracao", "ind_perm_alte", ParserDefinitions::Bit); //empty
				addColumnMapping(versDomiAcaoPref,	"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(dominioAcaoPref,		"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true,
				addColumnMapping(codClasseAcaoPref,			"NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt, true))); //empty
				addColumnMapping(versDomiAcaoPref, "IndicadorVersaoAtiva", "ind_vers_ativa", ParserDefinitions::Bit);//empty
			addColumnMapping(dominioAcaoPref, "CodigoTipoUtilizadorDominio", "cod_tipo_util_domi", ParserDefinitions::SmallInt); //empty
			addColumnMapping(dominioAcaoPref,	"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true,
			addColumnMapping(codClasseAcaoPref,		"CodigoOpcao", "cod_opc_domi", ParserDefinitions::SmallInt, true)); //empty
		addColumnMapping(codClasseAcaoPref, "SiglaOpcaoDominio", "sigl_opc_domi", ParserDefinitions::VarChar); //empty
		addColumnMapping(codClasseAcaoPref, "DescricaoOpcaoDominio", "desc_opc_domi", ParserDefinitions::VarChar); //empty
		addColumnMapping(codClasseAcaoPref, "DescricaoPluralOpcaoDominio", "desc_plur_opc_domi", ParserDefinitions::VarChar); //empty

	addColumnMapping(mainTable, "ValorProventoPorAcao", "val_prov_acao", ParserDefinitions::Numeric); //
}
