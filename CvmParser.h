#ifndef CVMPARSER_H
#define CVMPARSER_H

#include <QtGlobal>
#include <QSqlDatabase>

class QString;
class QByteArray;
class QBuffer;
class QSqlTableModel;
class Parser;
class QuaZip;
class QuaZipFile;

class CvmParser
{
public:
private:
	QSqlDatabase	dbConnection;
	quint32			downloadID;
	QString			filePath;

public:
	void			setDatabase(QSqlDatabase &dbConnection) { this->dbConnection = dbConnection; }
	QSqlDatabase	&getDatabase() { return dbConnection; }
	const QString	&getFilePath() const { return filePath; }
	quint32			getDownloadID() const { return this->downloadID; }

private:
	QBuffer			*fileBuffer;
	QByteArray		*fileData;
	QSqlTableModel	*tableModel;

	bool			updateSQLDatabase(const Parser *parser);
	bool			cleanupSQLDatabase();
	int				getFileVersion(QuaZip &zip);
	int				getOldFileFormatVersion(QuaZip &zip);
	int				getNewFileFormatVersion(QuaZip &zip);

	bool			setParserAsActive(bool active = true);
	bool			parseOldVersion(QuaZip &zip, int version);
	bool			parseNewVersion(QuaZip &zip, int version);
	QuaZipFile		*getMainFile(QuaZip *zip, int version) const;

public:
	CvmParser(const QString &fileName, QSqlDatabase &db, quint32 downloadID);
	CvmParser(QByteArray &fileData, QSqlDatabase &db, quint32 downloadID);
	~CvmParser();

public:
	bool			parse();
	static bool		cleanupSQLDatabase(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMPARSER_H
