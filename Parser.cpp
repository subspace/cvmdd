#include "Parser.h"
#include "ParserDefinitions.h"
#include "ParserFixup.h"
#include <QStringList>
#include <QDateTime>
#include <QFile>

#include <QDebug>

Parser::~Parser()
{
}

const QString &Parser::name() const
{
	return defs->name();
}

int Parser::fieldCount() const {
	return defs->size();
}

const QString &Parser::fieldName(int fieldIndex) const
{
	return defs->fieldName(fieldIndex);
}

void Parser::setFile(QFile *file)
{
	this->parserFile = file;

	fileData = QString(parserFile->readAll().data());
}

void Parser::setData(const QString &fileData)
{
	if (parserFile) {
		parserFile = NULL;
	}
	this->fileData = fileData;
}

bool Parser::parse()
{
	lineCount = 0;
	parsedLines.clear();

	if (!defs)
		return false;

	QStringList fileRows = fileData.split("\n", QString::SkipEmptyParts);

	for (int i = 0; i < fileRows.size(); i++) {
		QString		thisRow = fileRows.at(i);
		quint32		pos = 0;
		ParsedLine	parsedLine;

		parsedLine.clear();

		//apply hacks
		if (defs->fixup()) {
			if (!defs->fixup()->fixup(thisRow))
				return false;
		}

		if (thisRow.size() < defs->size()) { //row size mismatch
			qDebug() << "Row size mismatch." << defs->size() << thisRow.size();
			return false;
		}

		for (int j = 0; j < defs->size(); j++) {
			ParsedField pf;
			QString		value;

			pf.name = defs->fieldName(j);
			value = thisRow.mid(pos, defs->fieldSize(j)).trimmed();

			//apply hacks
			if (defs->fieldFixup(j)) {
				if (!defs->fieldFixup(j)->fixup(value))
					return false;
			}
			QDateTime	dateTime;
			QTime		time;

			switch (defs->fieldType(j)) {
			case ParserDefinitions::Text:
			case ParserDefinitions::NText:
			case ParserDefinitions::VarChar:
			case ParserDefinitions::Char:
			case ParserDefinitions::NVarChar:
			case ParserDefinitions::NChar:
				pf.value = QVariant(value.replace("'", ""));
				break;
			case ParserDefinitions::TinyInt:
			case ParserDefinitions::SmallInt:
			case ParserDefinitions::Int:
			case ParserDefinitions::BigInt:
				pf.value = QVariant(value.replace(" ", "").toLongLong());
				break;
			case ParserDefinitions::Real:
			case ParserDefinitions::Float:
			case ParserDefinitions::Money:
			case ParserDefinitions::Decimal:
			case ParserDefinitions::Numeric:
			case ParserDefinitions::SmallMoney:
				value = value.replace(" ", "");
				value = value.replace(",", ".");
				pf.value = QVariant(value.toDouble());
				break;
			case ParserDefinitions::Bit:
				value = value.replace("S", "1");
				value = value.replace("N", "0");
				pf.value = QVariant(QVariant(value.toInt()).toBool());
				break;
			case ParserDefinitions::DateTime:
			case ParserDefinitions::SmallDateTime:
				dateTime = QDateTime::fromString(value, defs->getFormat(j));
				if (!dateTime.isValid())
					pf.value = QVariant(QDateTime());
				else
					pf.value = QVariant(dateTime);
				break;
			case ParserDefinitions::Time:
				time = QTime::fromString(value, defs->getFormat(j));
				if (!time.isValid())
					pf.value = QVariant(QTime());
				else
					pf.value = QVariant(time);
				break;
			case ParserDefinitions::ByteArray:
				pf.value = QVariant(QByteArray::fromBase64(value.toAscii()));
				break;
			case ParserDefinitions::None:
				break;
			}
//			qDebug() << defs->name() << pf.name << value << pf.value;
			parsedLine.push_back(pf);

			pos += defs->fieldSize(j);
		}

		parsedLines.push_back(parsedLine);

		lineCount++;
	}
	return true;
}

const QVariant &Parser::value(int row, const QString &fieldName) const
{
	static QVariant e;

	if (row >= lineCount)
		return e;

	const ParsedLine	*pl = &parsedLines.at(row);
	const ParsedField	*pf;

	for (int i = 0; i < pl->size(); i++) {
		pf = &pl->at(i);
		if (pf->name == fieldName)
			return pf->value;
	}

	return e;
}

const QVariant &Parser::value(int row, int fieldIndex) const
{
	static QVariant e;

	if (row >= lineCount)
		return e;

	const ParsedLine	*pl = &parsedLines.at(row);
	const ParsedField	*pf;

	if (fieldIndex >= pl->size())
		return e;

	pf = &pl->at(fieldIndex);

	return pf->value;
}

