#include "CvmXmlHandlerInformFinanDemonstracaoFinan.h"

CvmXmlHandlerInformFinanDemonstracaoFinan::CvmXmlHandlerInformFinanDemonstracaoFinan(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *infodfin = addTableMapping("InformacaoFinanceiraDemonstracaoFinanceira", "tfreinfo_fina_dfin");
	addColumnMapping(infodfin, "NumeroIdentificadorInfoFinaDFin", "num_idt_info_fina_dfin", ParserDefinitions::Int, true); //

	XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin", infodfin);
		XmlSqlTableMap *documento = addTableMapping("Documento",	"tfredocm", form);
		addColumnMapping(documento,	"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
		addColumnMapping(form,		"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
		addColumnMapping(infodfin,	"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true)));//
		addColumnMapping(documento, "CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false,
		addColumnMapping(form,		"CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false,
		addColumnMapping(infodfin,	"CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false)));//

	XmlSqlTableMap *planoConta = addTableMapping("PlanoConta", "tfrepcon", infodfin);
		XmlSqlTableMap *versaoPlanoConta = addTableMapping("VersaoPlanoConta", "tfrevers_pcon", planoConta);
		addColumnMapping(versaoPlanoConta,	"CodigoTipoDemonstracaoFinanceira",	"cod_tipo_dfin",		ParserDefinitions::SmallInt, true,
		addColumnMapping(planoConta,		"CodigoTipoDemonstracaoFinanceira", "cod_tipo_dfin",		ParserDefinitions::SmallInt, true,
		addColumnMapping(infodfin,			"CodigoTipoDemonstracaoFinanceira", "cod_tipo_dfin",		ParserDefinitions::SmallInt)));//
		addColumnMapping(versaoPlanoConta,	"CodigoTipoInformacaoFinanceira",	"cod_tipo_info_fina",	ParserDefinitions::SmallInt, true,
		addColumnMapping(planoConta,		"CodigoTipoInformacaoFinanceira",	"cod_tipo_info_fina",	ParserDefinitions::SmallInt, true,
		addColumnMapping(infodfin,			"CodigoTipoInformacaoFinanceira",	"cod_tipo_info_fina",	ParserDefinitions::SmallInt)));//
		addColumnMapping(versaoPlanoConta,	"NumeroVersaoPlanoConta",			"num_vers_pcon",		ParserDefinitions::SmallInt, true,
		addColumnMapping(planoConta,		"NumeroVersaoPlanoConta",			"num_vers_pcon",		ParserDefinitions::SmallInt, true,
		addColumnMapping(infodfin,			"NumeroVersaoPlanoConta",			"num_vers_pcon",		ParserDefinitions::SmallInt)));//
	addColumnMapping(planoConta,	"NumeroConta",		"num_con",		ParserDefinitions::VarChar, true,
	addColumnMapping(infodfin,		"NumeroConta",		"num_con",		ParserDefinitions::VarChar)); // okay but there are more fields available


	XmlSqlTableMap *descconta = addTableMapping("DescricaoContaInformacaoFinanceiraDemonstracaoFinanceira", "tfredesc_con_info_fina_dfin", infodfin);
		XmlSqlTableMap *infodfin2 = addTableMapping("InformacaoFinanceiraDemonstracaoFinanceira", "tfreinfo_fina_dfin", descconta);
		addColumnMapping(infodfin2, "NumeroIdentificadorInfoFinaDFin", "num_idt_info_fina_dfin", ParserDefinitions::Int, true,
		addColumnMapping(descconta, "NumeroIdentificadorInfoFinaDFin", "num_idt_info_fina_dfin", ParserDefinitions::Int, true));//
		addColumnMapping(infodfin2, "DescricoesContaInformacaoFinanceiraDemonstracaoFinanceira");
		addColumnMapping(infodfin2, "ColunasInformacaoFinanceiraDemonstracaoFinanceira");

		XmlSqlTableMap *idioma = addTableMapping("Idioma", "tfreidio", descconta);
		addColumnMapping(idioma,	"CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true,
		addColumnMapping(descconta, "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true));
		addColumnMapping(descconta, "DescricaoConta", "desc_con", ParserDefinitions::VarChar);

	XmlSqlTableMap *colinfo = addTableMapping("ColunasInformacaoFinanceiraDemonstracaoFinanceira", "", infodfin);
		XmlSqlTableMap *colinfo2 = addTableMapping("ColunasInformacaoFinanceiraDemonstracaoFinanceira", "tfrecolu_info_fina_dfin", colinfo);
			XmlSqlTableMap *infodfin3 = addTableMapping("InformacaoFinanceiraDemonstracaoFinanceira", "tfreinfo_fina_dfin", colinfo2);
			addColumnMapping(infodfin3, "NumeroIdentificadorInfoFinaDFin", "num_idt_info_fina_dfin", ParserDefinitions::Int, true,
			addColumnMapping(colinfo2,	"NumeroIdentificadorInfoFinaDFin", "num_idt_info_fina_dfin", ParserDefinitions::Int, true));
			addColumnMapping(infodfin3, "DescricoesContaInformacaoFinanceiraDemonstracaoFinanceira");
			addColumnMapping(infodfin3, "ColunasInformacaoFinanceiraDemonstracaoFinanceira");

			XmlSqlTableMap *perdfin = addTableMapping("PeriodoDemonstracaoFinanceira", "tfreperi_dfin", colinfo2);
			addColumnMapping(perdfin,	"NumeroIdentificacaoPeriodo",	"num_idt_peri_dfin", ParserDefinitions::SmallInt, true,
			addColumnMapping(colinfo2,	"NumeroIndentificacaoPeriodo",	"num_idt_peri_dfin", ParserDefinitions::SmallInt, true));

			addColumnToColumnMapping(findColumnMap(infodfin, "NumeroSequencialDocumento"), addColumnMapping(colinfo2, "NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true));

			addColumnMapping(colinfo2, "NumeroIdentificacaoColuna", "num_idt_colu", ParserDefinitions::SmallInt, true);
			addColumnMapping(colinfo2, "ValorConta", "val_con", ParserDefinitions::Numeric);
			addColumnMapping(colinfo2, "DescricaoColunaGrid", "desc_col_grid", ParserDefinitions::VarChar);
}
