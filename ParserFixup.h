#ifndef PARSERFIXUP_H
#define PARSERFIXUP_H

#include <QString>

class Parser;
class ParserDefinitions;

class ParserFixup
{
public:
	ParserFixup(ParserDefinitions *defs);
	virtual bool fixup(QString &value) = 0;

private:
	ParserDefinitions *defs;
};

#endif // PARSERFIXUP_H
