#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlQuery>

namespace Ui {
	class MainWindow;
}

class CvmApplication;
class QSqlQueryModel;

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	void setApplication(CvmApplication *app) { this->app = app; }

private:
	Ui::MainWindow	*ui;
	CvmApplication	*app;
	QSqlQueryModel	*modelUndownloaded;
	QSqlQueryModel	*modelUnparsed;
	QSqlQueryModel	*modelParsed;
	QSqlQuery		queryUndownloaded;
	QSqlQuery		queryUnparsed;
	QSqlQuery		queryParsed;

private slots:
	void on_actionConfig_triggered();
	void on_actionExit_triggered() { this->close(); }
	void on_actionAbout_triggered();

	void on_request_clicked();
	void on_download_clicked();
	void on_parse_clicked();
	void on_pushClearFailed_clicked();

	void on_today_clicked(bool checked);
	void on_thisWeek_clicked(bool checked);
	void on_last30Days_clicked(bool checked);

	void refreshLists();
};

#endif // MAINWINDOW_H
