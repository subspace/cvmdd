#include "CvmXmlHandlerAnexoTexto.h"

CvmXmlHandlerAnexoTexto::CvmXmlHandlerAnexoTexto(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *anexoTexto = addTableMapping("AnexoTexto", "tfretxt_anex");
		//mapping only documento tag useful information
		XmlSqlTableMap *documento = addTableMapping("Documento", "tfredocm", anexoTexto);
		addColumnMapping(documento,		"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
		addColumnMapping(anexoTexto,	"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true));

	addColumnMapping(anexoTexto, "NumeroQuadroRelacionado", "num_qdro_relc", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoTexto, "NumeroCampoRelacionado", "num_camp_relc", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoTexto, "NumeroGrupoRelacionado", "num_grup_relc", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoTexto, "NumeroItemTextoLivre", "num_item_txt_livr", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoTexto, "NumeroSubCampo", "num_sub_camp", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoTexto, "Texto", "txt_desc", ParserDefinitions::VarChar);

		XmlSqlTableMap *idioma = addTableMapping("Idioma", "tfreidio", anexoTexto);
		addColumnMapping(idioma,	"CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true,
		addColumnMapping(anexoTexto,"CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true));
}
