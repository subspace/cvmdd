#ifndef CVMDOWNLOADER_H
#define CVMDOWNLOADER_H

#include "Downloader.h"

class CvmDownloader : public Downloader
{
	Q_OBJECT
public:
private:
	quint32 downloadID;
	static QList<CvmDownloader *> activeDownloads;

public:
	void	setDownloadID(quint32 downloadID) { this->downloadID = downloadID; }
	quint32 getDownloadID() const { return this->downloadID; }
	static int	getActiveDownloadCount() { return activeDownloads.size(); }

private:
protected slots:
	void downloadFinished() { Downloader::downloadFinished(); emit finished(this); activeDownloads.removeAll(this); }
	void downloadError(QNetworkReply::NetworkError err) { Downloader::downloadError(err); emit error(this, err); activeDownloads.removeAll(this); }
	void downloadProgress(qint64 bytesReceived, qint64 bytesTotal) { Downloader::downloadProgress(bytesReceived, bytesTotal); emit progress(this, bytesReceived, bytesTotal); }

public:
	CvmDownloader(quint32 downloadID, QNetworkAccessManager *manager, const QUrl &downloadUrl, const QDir &saveDir, const QString &saveFileName, bool saveToMemory, QObject *parent):
	Downloader(manager, downloadUrl, saveDir, saveFileName, saveToMemory, parent), downloadID(downloadID) {}
	~CvmDownloader() {}

public:
	bool startDownload() { activeDownloads.append(this); return Downloader::startDownload(); }

signals:
	void finished(CvmDownloader *downloader);
	void error(CvmDownloader *downloader, QNetworkReply::NetworkError err);
	void progress(CvmDownloader *downloader, qint64 bytesReceived, qint64 bytesTotal);

};

#endif // CVMDOWNLOADER_H
