#ifndef PARSER_H
#define PARSER_H

#include <QVariant>

class QFile;
class ParserDefinitions;
class QString;

class Parser
{
public:
private:
	QFile				*parserFile;
	QString				fileData;
	ParserDefinitions	*defs;
	int					lineCount;

public:
	void			setFile(QFile *file);
	QFile			*file() const { return parserFile; }
	void			setData(const QString &fileData);
	const QString	&data() { return this->fileData; }
	void			setDefinitions(ParserDefinitions *defs) { this->defs = defs; }
	const ParserDefinitions *definitions() const { return defs; }
	int				size() const { return lineCount; }

	const QVariant	&value(int row, const QString &fieldName) const;
	const QVariant	&value(int row, int fieldIndex) const;
	const QString	&fieldName(int fieldIndex) const;
	int				fieldCount() const;
	const QString	&name() const;

private:
	struct ParsedField {
		QString		name;
		QVariant	value;
	};
	typedef QList<ParsedField> ParsedLine;
	QList<ParsedLine> parsedLines;

public:
	Parser(const QString &fileData, ParserDefinitions *defs): parserFile(NULL), fileData(fileData), defs(defs), lineCount(0) {}
	Parser(QFile *file, ParserDefinitions *defs): defs(defs), lineCount(0) { setFile(file); }
	Parser(): parserFile(NULL), defs(NULL), lineCount(0) { fileData.clear(); } //set filedata and defs urself b4 using.
	virtual ~Parser();

public:
	bool			parse();

};

#endif // PARSER_H
