#ifndef CVMXMLHANDLER_H
#define CVMXMLHANDLER_H

#include <QXmlDefaultHandler>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QList>
#include <QPair>
#include "ParserDefinitions.h"

class QSqlTableModel;

class CvmXmlHandler : public QXmlDefaultHandler
{
protected:
	QSqlDatabase				db;

	struct XmlSqlTableMap {
	private:
		QSqlTableModel	*model;
		XmlSqlTableMap	*parentTable;
		QSqlRecord		record;
	public:
		QString			xmlTableName, sqlTableName;

		bool operator ==(const XmlSqlTableMap &other) {
			if (other.xmlTableName == this->xmlTableName && other.sqlTableName == this->sqlTableName)
				return true;
			else
				return false;
		}
		friend class CvmXmlHandler;
	};

	struct XmlSqlColumnMap {
	private:
		XmlSqlTableMap			*table;

	public:
		QString							xmlColumnName, sqlColumnName;
		ParserDefinitions::FieldType	type;
		bool							isPrimaryKey;
		QString							dateTimeFormat;

		XmlSqlColumnMap					*linkedColumn;

		void					setTable(XmlSqlTableMap *table) { this->table = table; }
		const XmlSqlTableMap	*getTable() { return this->table; }
		bool operator ==(const XmlSqlColumnMap &other) {
			if (other.sqlColumnName == this->sqlColumnName && other.xmlColumnName == this->xmlColumnName
				&& other.table == this->table)
				return true;
			else
				return false;
		}
		friend class CvmXmlHandler;
	};


	XmlSqlTableMap				*addTableMapping(const QString &xmlTableName, const QString &sqlTableName, XmlSqlTableMap *parentTable = NULL);
	XmlSqlColumnMap				*addColumnMapping(XmlSqlTableMap *table, const QString &xmlColumnName, const QString &sqlColumnName = "", ParserDefinitions::FieldType type = ParserDefinitions::None, bool isPrimaryKey = false, XmlSqlColumnMap *linkedColumn = NULL);
	XmlSqlColumnMap				*findColumnMap(const XmlSqlTableMap *table, const QString &xmlColumnName) const;
	bool						addColumnToColumnMapping(XmlSqlColumnMap *origin, XmlSqlColumnMap *dest);
	void						setColumnMappingDateTimeFormat(XmlSqlColumnMap *colMap, const QString &format) { colMap->dateTimeFormat = format; }

private:
	struct XmlSqlColumnToColumnMap {
		XmlSqlColumnMap *origin, *dest;
	};

	QList<XmlSqlTableMap *>				tableMap;
	QList<XmlSqlColumnMap *>			columnMap;
	QList<XmlSqlColumnToColumnMap *>	columnToColumnMaps;


	XmlSqlTableMap				*currentTable;
	QString						currentElementValue;
//	QList<QSqlTableModel *>		modifiedModels;
	quint32						downloadID;

	void						cleanupMappings();
	XmlSqlTableMap				*findTableMap(const QString &xmlTableName, XmlSqlTableMap *parentTable = NULL) const;

	bool						startDocument();
	bool						endDocument();
	bool						startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts);
	bool						endElement(const QString &namespaceURI, const QString &localName, const QString &qName);
	bool						characters(const QString &ch);

	bool						initMappings();

	void						dumpModel(QSqlTableModel *model) const;
	bool						checkConstraint(XmlSqlTableMap *table) const;
	void						linkedColumnRecursiveSet(XmlSqlColumnMap *column);

public:
	void						setDatabase(QSqlDatabase &db) { this->db = db; }
	QSqlDatabase				&getDatabase() { return this->db; }

public:
	CvmXmlHandler(QSqlDatabase &db, quint32 downloadID);

	virtual ~CvmXmlHandler();
};

#endif // CVMXMLHANDLER_H
