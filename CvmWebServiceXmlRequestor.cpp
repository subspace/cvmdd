#include "CvmWebServiceXmlRequestor.h"
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QSslConfiguration>
#include <QUrl>
#include <QByteArray>

QUrl CvmWebServiceXmlRequestor::cvmUrl[2] = {
	QUrl("https://www.rad.cvm.gov.br/download/SolicitaDownload.asp"),
	QUrl("https://siteseguro.bovespa.com.br/rad/download/SolicitaDownload.asp")
};
QList<CvmWebServiceXmlRequestor *> CvmWebServiceXmlRequestor::activeRequests;

CvmWebServiceXmlRequestor::CvmWebServiceXmlRequestor(QNetworkAccessManager *manager, QObject *parent): QObject(parent), manager(manager)
{
	requesting = false;
}

CvmWebServiceXmlRequestor::~CvmWebServiceXmlRequestor()
{
	activeRequests.removeAll(this);
}

bool CvmWebServiceXmlRequestor::request()
{
	if (requesting)
		return false;
	requesting = true;

	activeRequests.push_back(this);

	QByteArray		postData;
	QNetworkRequest req(cvmUrl[0]);

	postData.append("txtLogin=" + username);
	postData.append("&txtSenha=" + password);
	postData.append("&txtData=" + date.toString("dd/MM/yyyy"));
	postData.append("&txtHora=" + time.toString("hh:mm"));
	postData.append("&txtDocumento=");
	postData.append(getDocTypeString(doc));
	postData.append("&txtAssuntoIPE=NAO");

	QSslConfiguration sslconf;
	sslconf.setProtocol(QSsl::SslV3);
	sslconf.setPeerVerifyMode(QSslSocket::VerifyNone);
	sslconf.setPeerVerifyDepth(2);
	req.setSslConfiguration(sslconf);

	reply = manager->post(req, postData);

	connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(requestError(QNetworkReply::NetworkError)));
	connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(requestProgress(qint64,qint64)));
	connect(reply, SIGNAL(finished()), this, SLOT(requestFinished()));
	connect(reply, SIGNAL(destroyed()), this, SLOT(replyDestroyed()));

	return true;
}

void CvmWebServiceXmlRequestor::requestProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	emit progress(this, bytesReceived, bytesTotal);
}

void CvmWebServiceXmlRequestor::requestError(QNetworkReply::NetworkError err)
{
	emit error(this, err);
	reply->deleteLater();
}

void CvmWebServiceXmlRequestor::requestFinished()
{
	emit finished(this, reply->readAll());
	reply->deleteLater();
}

void CvmWebServiceXmlRequestor::replyDestroyed()
{
	requesting = false;
	activeRequests.removeAll(this);

}

CvmWebServiceXmlRequestor::DocType CvmWebServiceXmlRequestor::getDocTypeFromString(const QString &string)
{
	if (string ==  "ITR")
		return CvmWebServiceXmlRequestor::ITR;
	else if (string == "DFP")
		return CvmWebServiceXmlRequestor::DFP;
	else if (string == "IAN")
		return CvmWebServiceXmlRequestor::IAN;
	else if (string == "RAD")
		return CvmWebServiceXmlRequestor::RAD;
	else if (string == "IPE")
		return CvmWebServiceXmlRequestor::IPE;
	else if (string == "ENET")
		return CvmWebServiceXmlRequestor::ENET;
	else if (string == "FRE")
		return CvmWebServiceXmlRequestor::FRE;
	else if (string == "FCA")
		return CvmWebServiceXmlRequestor::FCA;
	else if (string == "TODOS")
		return CvmWebServiceXmlRequestor::TODOS;
	else
		return CvmWebServiceXmlRequestor::Unknown;
}

QString CvmWebServiceXmlRequestor::getDocTypeString(DocType docType)
{
	switch(docType) {
	case CvmWebServiceXmlRequestor::ITR:
		return "ITR";
	case CvmWebServiceXmlRequestor::DFP:
		return "DFP";
	case CvmWebServiceXmlRequestor::IAN:
		return "IAN";
	case CvmWebServiceXmlRequestor::RAD:
		return "RAD";
	case CvmWebServiceXmlRequestor::IPE:
		return "IPE";
	case CvmWebServiceXmlRequestor::ENET:
		return "ENET";
	case CvmWebServiceXmlRequestor::FRE:
		return "FRE";
	case CvmWebServiceXmlRequestor::FCA:
		return "FCA";
	case CvmWebServiceXmlRequestor::TODOS:
		return "TODOS";
	case CvmWebServiceXmlRequestor::Unknown:
		return "Unknown";
	}
	return "Unknown";
}
