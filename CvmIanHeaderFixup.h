#ifndef CVMFIXUP_H
#define CVMFIXUP_H

#include "ParserFixup.h"

class CvmIanHeaderFixup : public ParserFixup
{
public:
	CvmIanHeaderFixup(ParserDefinitions *defs): ParserFixup(defs) {}
	bool fixup(QString &value);
};

#endif // CVMFIXUP_H
