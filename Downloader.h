#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QObject>
#include <QDir>
#include <QUrl>
#include <QNetworkReply>
#include <QDebug>

class QNetworkAccessManager;

class Downloader : public QObject
{
	Q_OBJECT
public:
private:
	QNetworkAccessManager *manager;
	QUrl				downloadUrl;
	QDir				saveDirectory;
	QString				saveFileName;
	QByteArray			fileInMemory;
	bool				saveToMemory;
	bool				downlFinished;

public:
	void				setDownloadUrl(const QUrl &downloadUrl) { this->downloadUrl = downloadUrl; }
	void				setSaveDirectory(const QDir &saveDir) { this->saveDirectory = saveDir; }
	void				setSaveFileName(const QString &fileName) { this->saveFileName = fileName; }
	void				setSaveToMemory(bool saveToMemory = false) { this->saveToMemory = saveToMemory; }
	void				setNetworkManager(QNetworkAccessManager *manager) { this->manager = manager; }
	const QUrl			&getDownloadUrl() const { return this->downloadUrl; }
	const QDir			&getSaveDirectory() const { return this->saveDirectory; }
	const QString		&getSaveFileName() const { return this->saveFileName; }
	const QString		getAbsoluteSaveFileName() const { return this->saveDirectory.absoluteFilePath(this->saveFileName); }
	const QByteArray	&getFileData() const { return this->fileInMemory; }
	QNetworkAccessManager *getNetworkManager() const { return this->manager; }
	bool				isSaveToMemorySet() const { return this->saveToMemory; }
	bool				isDownloadFinished() const { return this->downlFinished; }

private:
	QNetworkReply		  *reply;

protected slots:
	virtual void		downloadFinished();
	virtual void		downloadError(QNetworkReply::NetworkError err);
	virtual void		downloadProgress(qint64 bytesReceived, qint64 bytesTotal);

public:
	explicit			Downloader(QNetworkAccessManager *manager, const QUrl &downloadUrl, const QDir &saveDir = QDir("."), const QString &saveFileName = "", bool saveToMemory = false, QObject *parent = 0);
	virtual				~Downloader();

public:
	bool				startDownload();
	void				cancelDownload();

signals:
	void				finished(Downloader *downloader);
	void				error(Downloader *downloader, QNetworkReply::NetworkError err);
	void				progress(Downloader *downloader, qint64 bytesReceived, qint64 bytesTotal);
};

#endif // DOWNLOADER_H
