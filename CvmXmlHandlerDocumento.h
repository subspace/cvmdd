#ifndef CVMXMLHANDLERDOCUMENTO_H
#define CVMXMLHANDLERDOCUMENTO_H

#include "CvmXmlHandler.h"

class QSqlTableModel;

class CvmXmlHandlerDocumento : public CvmXmlHandler
{
public:
	CvmXmlHandlerDocumento(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERDOCUMENTO_H
