#include "CvmCTRHoraEmissFixup.h"

CvmCTRHoraEmissFixup::CvmCTRHoraEmissFixup(ParserDefinitions *defs): ParserFixup(defs)
{
}

bool CvmCTRHoraEmissFixup::fixup(QString &value)
{
	if (value.size() == 8 && value.at(2) == ':' && value.at(5) == ':') { //field doesn't need a hack
		value = value.replace(":", "");
		return true;
	}

	QString hour, minute, second;
	int idx, lastIndex;

	value = value.replace("/", ":");

	//fix time without zeros in front
	if (value.at(0).isDigit()) {
		idx = value.indexOf(":", 0)+1;
		hour = "0" + value.mid(0, idx);
		hour = hour.right(2).trimmed();
		lastIndex = idx;

		idx = value.indexOf(":", lastIndex)+1;
		minute = "0" + value.mid(lastIndex, idx - lastIndex).trimmed();
		minute = minute.right(2);
		lastIndex = idx;

		idx = value.indexOf(":", lastIndex)+1;
		second = "0" + value.mid(lastIndex, idx - lastIndex).trimmed();
		second = second.right(2);
		lastIndex = idx;

		value = hour + minute + second;

		return true;
	}

	//AM PM fixup
	if ((value.at(0) == 'P' || value.at(0) == 'A') && value.at(1) == 'M') {
		idx = value.indexOf(":", 3)+1;
		hour = value.mid(3, idx-3).trimmed();
		if (hour.size() > 2)
			return false;
		if (value.at(0) == 'P') {
			if (hour.toInt() == 12)
				hour = "00";
			else
				hour = QString::number(hour.toInt()+12);
		}
		minute = value.mid(idx, value.indexOf(":", idx)+1 - idx).trimmed();

		hour = QString("0" + hour).right(2);
		minute = QString("0" + minute).right(2);
		value = hour + minute + "00";

		return true;
	}
	return true;
}
