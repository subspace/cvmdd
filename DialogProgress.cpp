#include "DialogProgress.h"
#include "ui_DialogProgress.h"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTimer>
#include <QCloseEvent>
#include <QDebug>

DialogProgress::DialogProgress(QWidget *parent) :
QDialog(parent),
ui(new Ui::DialogProgress)
{
	model = new QStandardItemModel(this);
	model->setObjectName("model");

	timer = new QTimer(this);
	timer->setObjectName("timer");

	ui->setupUi(this);

	ui->tableView->setModel(model);
  ui->tableView->horizontalHeader()->setStretchLastSection(true);

	getActiveCount = 0;

	timer->start(100);
}

void DialogProgress::closeEvent(QCloseEvent *evt)
{
	if (ui->buttonBox->isEnabled())
		evt->accept();
	else
		evt->ignore();
}

void DialogProgress::on_timer_timeout()
{
	if (getActiveCount) {
		if (getActiveCount()) {
			ui->buttonBox->setEnabled(false);
		} else {
			ui->buttonBox->setEnabled(true);
			timer->stop();
		}
	}
}

void DialogProgress::updateProgress(unsigned int id, const QString &desc, const QString &status)
{
	QMutexLocker locker(&mutex);

	for (int i = 0; i < model->rowCount(); i++) {
		QModelIndex idxID = model->index(i, 0);
		QModelIndex idxDesc = model->index(i, 1);
		QModelIndex idxStatus = model->index(i, 2);

		if (id == model->data(idxID, Qt::UserRole+1).toUInt()) {
			model->setData(idxDesc, desc);
			model->setData(idxStatus, status);
      ui->tableView->resizeColumnToContents(1);
			return;
		}
	}

	qDebug() << desc << status;
	QList<QStandardItem *>	items;
	QStandardItem			*idItem = new QStandardItem();
	QStandardItem			*descItem = new QStandardItem(desc);
	QStandardItem			*statusItem = new QStandardItem(status);

	idItem->setData(id);
	items.push_back(idItem);
	items.push_back(descItem);
	items.push_back(statusItem);

	model->insertRow(model->rowCount(), items);
	model->setHeaderData(0, Qt::Horizontal, "ID");
	model->setHeaderData(1, Qt::Horizontal, "Description");
	model->setHeaderData(2, Qt::Horizontal, "Status");

//	ui->tableView->resizeColumnsToContents();
	ui->tableView->hideColumn(0);
}

DialogProgress::~DialogProgress()
{
	delete ui;
}
