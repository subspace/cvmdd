#ifndef CVMITRHEADERFIXUP_H
#define CVMITRHEADERFIXUP_H

#include "ParserFixup.h"

class CvmItrHeaderFixup : public ParserFixup
{
public:
	CvmItrHeaderFixup(ParserDefinitions *defs): ParserFixup(defs) {}
	bool fixup(QString &value);
};

#endif // CVMITRHEADERFIXUP_H
