#include "CvmXmlHandlerCompCapitSocDemonsFinanNegocios.h"

CvmXmlHandlerCompCapitSocDemonsFinanNegocios::CvmXmlHandlerCompCapitSocDemonsFinanNegocios(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *comp = addTableMapping("ComposicaoCapitalSocialDemonstracaoFinanceira", "tfrecoms_cpsc_dfin");
		//periodo demonstracao financeira
		XmlSqlTableMap *perdfin = addTableMapping("PeriodoDemonstracaoFinanceira", "tfreperi_dfin", comp);
			addColumnMapping(perdfin,	"NumeroIdentificacaoPeriodo", "num_idt_peri_dfin", ParserDefinitions::SmallInt, true,
			addColumnMapping(comp,		"NumeroIdentificacaoPeriodo", "num_idt_peri_dfin", ParserDefinitions::SmallInt));

		//formulario demonstracao financeira
		XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin", comp);
			XmlSqlTableMap *documento = addTableMapping("Documento", "tfredocm", form);
			addColumnMapping(documento,	"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
			addColumnMapping(form,		"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
			addColumnMapping(comp,		"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int)));

	addColumnMapping(comp, "NumeroIdentificacaoComposicaoCapitalSocial",	"num_idt_coms_cpsc", ParserDefinitions::Int, true);
	addColumnMapping(comp, "QuantidadeAcaoOrdinariaCapitalIntegralizado",	"qte_aord_capi_itgz", ParserDefinitions::BigInt);
	addColumnMapping(comp, "QuantidadeAcaoPreferencialCapitalIntegralizado","qte_aprf_capi_itgz", ParserDefinitions::BigInt);
	addColumnMapping(comp, "QuantidadeTotalAcaoCapitalIntegralizado",		"qte_tot_acao_capi_itgz", ParserDefinitions::BigInt);
	addColumnMapping(comp, "QuantidadeAcaoOrdinariaTesouraria",				"qte_aord_teso", ParserDefinitions::BigInt);
	addColumnMapping(comp, "QuantidadeAcaoPreferencialTesouraria",			"qte_aprf_teso", ParserDefinitions::BigInt);
	addColumnMapping(comp, "QuantidadeTotalAcaoTesouraria",					"qte_tot_acao_teso", ParserDefinitions::BigInt);
}
