#include "CvmParserThreadStarter.h"
#include "CvmParser.h"
#include <QSettings>
#include <QSemaphore>
#include <QDebug>

QList<CvmParser *>	CvmParserThreadStarter::activeParsers;
QMutex				CvmParserThreadStarter::activeParsersMutex;

CvmParserThreadStarter::CvmParserThreadStarter(QSemaphore *pool, const QString &fileName, quint32 downloadID, QObject *parent) :
QThread(parent), fileName(fileName), downloadID(downloadID), pool(pool)
{
	connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
	connect(this, SIGNAL(terminated()), this, SLOT(threadTerminated()));
}

void CvmParserThreadStarter::threadTerminated()
{
	emit parsingTerminated(parser);
	activeParsers.removeAll(parser);
	deleteLater();
}

bool CvmParserThreadStarter::openDB()
{
	QSettings		set;
	db = QSqlDatabase::addDatabase(set.value("db_type").toString(), QString::number(downloadID));
	db.setHostName(set.value("db_address").toString());
	db.setPort(set.value("db_port").toInt());
	db.setDatabaseName(set.value("db_name").toString());
	db.setUserName(set.value("db_username").toString());
	db.setPassword(set.value("db_password").toString());
	return db.open();
}

void CvmParserThreadStarter::run()
{
	pool->acquire();

	if (!openDB())
		emit parsingFinished(parser, false);
	parser = new CvmParser(fileName, db, downloadID);

	activeParsersMutex.lock();
		activeParsers.push_back(parser);
	activeParsersMutex.unlock();

	bool r = parser->parse();
	emit parsingFinished(parser, r);
}

CvmParserThreadStarter::~CvmParserThreadStarter()
{
	pool->release();
	activeParsers.removeAll(parser);
}
