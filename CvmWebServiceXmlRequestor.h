#ifndef CVMXMLREQUESTOR_H
#define CVMXMLREQUESTOR_H

#include <QString>
#include <QDate>
#include <QTime>
#include <QUrl>
#include <QDebug>
#include <QNetworkReply>

class QNetworkAccessManager;

class CvmWebServiceXmlRequestor: public QObject
{
	Q_OBJECT

public:
	enum DocType { ITR, DFP, IAN, RAD, IPE, ENET, FRE, FCA, TODOS, Unknown };

private:
	QString					username, password;
	QDate					date;
	QTime					time;
	DocType					doc;
	QByteArray				xmlResponse;

public:
	void					setUsername(const QString &username) { this->username = username; }
	void					setPassword(const QString &password) { this->password = password; }
	void					setDate(const QDate &date) { this->date = date; }
	void					setTime(const QTime &time) { this->time = time; }
	void					setDocType(DocType type) { this->doc = type; }
	void					setNetworkManager(QNetworkAccessManager *manager) { this->manager = manager; }

	const QString			&getUsername() const { return this->username; }
	const QDate				&getDate() const { return this->date; }
	const QTime				&getTime() const { return this->time; }
	DocType					getDocType() const { return this->doc; }
	const QByteArray		&getXml() const { return this->xmlResponse; }
	QNetworkAccessManager	*getNetworkManager() const { return this->manager; }

	static QString			getDocTypeString(DocType docType);
	static DocType			getDocTypeFromString(const QString &string);

private:
	static QUrl				cvmUrl[2];
	static QList<CvmWebServiceXmlRequestor *> activeRequests;
	QNetworkAccessManager	*manager;
	QNetworkReply			*reply;
	bool					requesting;

private slots:
	void					requestFinished();
	void					requestProgress(qint64 bytesReceived, qint64 bytesTotal);
	void					requestError(QNetworkReply::NetworkError err);
	void					replyDestroyed();

public:
	CvmWebServiceXmlRequestor(QNetworkAccessManager *manager, QObject *parent = 0);
	~CvmWebServiceXmlRequestor();

public:
	bool					request();
	static int				getActiveRequests() { return activeRequests.size(); }

signals:
	void					progress(CvmWebServiceXmlRequestor *request, qint64 bytesReceived, qint64 bytesTotal);
	void					finished(CvmWebServiceXmlRequestor *request, const QByteArray &xmlResponse);
	void					error(CvmWebServiceXmlRequestor *request, QNetworkReply::NetworkError error);
};

#endif // CVMXMLREQUESTOR_H
