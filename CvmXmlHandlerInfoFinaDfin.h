#ifndef CVMXMLHANDLERINFOFINADFIN_H
#define CVMXMLHANDLERINFOFINADFIN_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerInfoFinaDfin : public CvmXmlHandler
{
public:
	CvmXmlHandlerInfoFinaDfin(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERINFOFINADFIN_H
