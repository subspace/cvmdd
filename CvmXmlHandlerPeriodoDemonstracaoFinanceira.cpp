#include "CvmXmlHandlerPeriodoDemonstracaoFinanceira.h"

CvmXmlHandlerPeriodoDemonstracaoFinanceira::CvmXmlHandlerPeriodoDemonstracaoFinanceira(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *perdfin = addTableMapping("PeriodoDemonstracaoFinanceira", "tfreperi_dfin");
		XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin");
			//mapping only documento tag useful information
			XmlSqlTableMap *documento = addTableMapping("Documento", "tfredocm");
			addColumnMapping(documento,			"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
				addColumnMapping(form,			"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
					addColumnMapping(perdfin,	"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true)));//

	addColumnMapping(perdfin, "NumeroIdentificacaoPeriodo", "num_idt_peri_dfin", ParserDefinitions::SmallInt, true);
	addColumnMapping(perdfin, "DataInicioPeriodo", "data_inic_peri_dfin", ParserDefinitions::DateTime);
	addColumnMapping(perdfin, "DataFimPeriodo", "data_fim_peri_dfin", ParserDefinitions::DateTime);
	addColumnMapping(perdfin, "NumeroTrimestre", "num_trim_dfin", ParserDefinitions::SmallInt);
}
