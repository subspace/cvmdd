#include "CvmXmlHandlerResumInfoFinanDemoFinan.h"

CvmXmlHandlerResumInfoFinanDemoFinan::CvmXmlHandlerResumInfoFinanDemoFinan(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *resumo = addTableMapping("ResumoInformacaoFinanceiraDemonstracaoFinanceira", "tfreresu_info_fina_dfin");
		XmlSqlTableMap *periodo = addTableMapping("PeriodoDemonstracaoFinanceira", "tfreperi_dfin", resumo);
			XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin", periodo);
				XmlSqlTableMap *doc = addTableMapping("Documento", "tfredocm", form);
				addColumnMapping(doc,		"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
				addColumnMapping(form,		"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
				addColumnMapping(periodo,	"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
				addColumnMapping(resumo,	"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true)))); //
		addColumnMapping(periodo,	"NumeroIdentificacaoPeriodo", "num_idt_peri_dfin", ParserDefinitions::SmallInt, true,
		addColumnMapping(resumo,	"NumeroIdentificacaoPeriodo", "num_idt_peri_dfin", ParserDefinitions::SmallInt, true));//
		XmlSqlTableMap *resumpcon = addTableMapping("ResumoPlanoConta", "tfreresu_pcon", resumo);
		addColumnMapping(resumpcon, "NumeroIdentificacaoResumoPlanoConta", "num_idt_resu_pcon", ParserDefinitions::Int, true,
		addColumnMapping(resumo,	"NumeroIdentificacaoResumoPlanoConta", "num_idt_resu_pcon", ParserDefinitions::Int, true));//
	addColumnMapping(resumo, "ValorResumo", "val_resu", ParserDefinitions::Numeric);//
}
