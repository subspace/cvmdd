#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "DialogConfig.h"
#include "DialogAbout.h"
#include "DialogProgress.h"
#include "CvmApplication.h"
#include "CvmDownloader.h"
#include "CvmParserThreadStarter.h"
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
	modelUndownloaded = new QSqlQueryModel(this);
	modelUndownloaded->setObjectName("modelUndownloaded");
	modelUnparsed = new QSqlQueryModel(this);
	modelUnparsed->setObjectName("modelUnparsed");
	modelParsed = new QSqlQueryModel(this);
	modelParsed->setObjectName("modelParsed");

	ui->setupUi(this);

	ui->all->setChecked(true);

	QDate today = QDate::currentDate();
	ui->today->setChecked(true);
	ui->startDate->setDate(today);
	ui->endDate->setDate(today);

	ui->undownloadedList->setModel(modelUndownloaded);
	ui->unparsedList->setModel(modelUnparsed);
	ui->parsedList->setModel(modelParsed);

	queryUndownloaded.prepare("SELECT dlcodcvmid \"Codigo CVM\", dltipodocumento \"Tipo Documento\", dldataref \"Data\" FROM downloads WHERE dldownloaded = false ORDER BY download_id DESC");
	queryUnparsed.prepare("SELECT download_id \"Download ID\", dlcodcvmid \"Codigo CVM\", dltipodocumento \"Tipo Documento\", dldataref \"Data\" FROM downloads WHERE dldownloaded = true AND dlparsed = false ORDER BY download_id DESC");
	queryParsed.prepare("SELECT dlcodcvmid \"Codigo CVM\", dltipodocumento \"Tipo Documento\", dldataref \"Data\" FROM downloads WHERE dlparsed = true ORDER BY download_id DESC");

	refreshLists();
}

void MainWindow::refreshLists()
{
	queryUndownloaded.exec();
	queryUnparsed.exec();
	queryParsed.exec();

	modelUndownloaded->setQuery(queryUndownloaded);
	modelUnparsed->setQuery(queryUnparsed);
	modelParsed->setQuery(queryParsed);

	ui->undownloadedList->resizeColumnsToContents();
	ui->unparsedList->resizeColumnsToContents();
	ui->parsedList->resizeColumnsToContents();
}

void MainWindow::on_actionConfig_triggered()
{
	DialogConfig *dlg = new DialogConfig(this);
	dlg->exec();
	delete dlg;
}

void MainWindow::on_actionAbout_triggered()
{
	DialogAbout *dlg = new DialogAbout(this);
	dlg->exec();
	delete dlg;
}

void MainWindow::on_request_clicked()
{
	CvmWebServiceXmlRequestor::DocType docType = CvmWebServiceXmlRequestor::Unknown;

	if (ui->itr->isChecked())
		docType = CvmWebServiceXmlRequestor::ITR;
	else if (ui->dfp->isChecked())
		docType = CvmWebServiceXmlRequestor::DFP;
	else if (ui->ian->isChecked())
		docType = CvmWebServiceXmlRequestor::IAN;
	else if (ui->fre->isChecked())
		docType = CvmWebServiceXmlRequestor::FRE;
	else if (ui->fca->isChecked())
		docType = CvmWebServiceXmlRequestor::FCA;
	else if (ui->all->isChecked())
		docType = CvmWebServiceXmlRequestor::TODOS;

	ui->statusbar->showMessage("Requesting XMLs...");

	DialogProgress *dp = new DialogProgress(this);
	dp->setActiveCheckFunction(CvmWebServiceXmlRequestor::getActiveRequests);
	connect(app, SIGNAL(progressUpdate(unsigned int,QString,QString)), dp, SLOT(updateProgress(unsigned int,QString,QString)));
	app->requestXMLs(ui->startDate->date(), ui->endDate->date(), docType);
	dp->exec();
	delete dp;

	ui->statusbar->showMessage("Requesting finished...", 7000);

	refreshLists();
}

void MainWindow::on_today_clicked(bool checked)
{
	if (checked) {
		ui->startDate->setDate(QDate::currentDate());
		ui->endDate->setDate(QDate::currentDate());
	}
}

void MainWindow::on_thisWeek_clicked(bool checked)
{
	if (checked) {
		ui->startDate->setDate(QDate::currentDate().addDays(-7));
		ui->endDate->setDate(QDate::currentDate());
	}
}

void MainWindow::on_last30Days_clicked(bool checked)
{
	if (checked) {
		ui->startDate->setDate(QDate::currentDate().addDays(-30));
		ui->endDate->setDate(QDate::currentDate());
	}
}

void MainWindow::on_download_clicked()
{
	ui->statusbar->showMessage("Downloading files... Please wait...");

	DialogProgress *dp = new DialogProgress(this);
	dp->setActiveCheckFunction(CvmDownloader::getActiveDownloadCount);
	connect(app, SIGNAL(progressUpdate(uint,QString,QString)), dp, SLOT(updateProgress(uint,QString,QString)));
	app->downloadPendingFiles();
	dp->exec();
	delete dp;

	ui->statusbar->showMessage("Download finished...", 7000);

	refreshLists();
}

void MainWindow::on_parse_clicked()
{
	ui->statusbar->showMessage("Parsing files...");

	DialogProgress *dp = new DialogProgress(this);
	dp->setActiveCheckFunction(CvmParserThreadStarter::getActiveParserCount);
	connect(app, SIGNAL(progressUpdate(uint,QString,QString)), dp, SLOT(updateProgress(uint,QString,QString)));
	app->parseDownloadedFiles();
	dp->exec();
	delete dp;

	ui->statusbar->showMessage("Parsing finished...", 7000);

	refreshLists();
}

void MainWindow::on_pushClearFailed_clicked()
{
	QSqlQuery sql;

	sql.prepare("SELECT dlfilepath, download_id FROM downloads WHERE parsing_failed = true");
	if (!sql.exec()) {
		QMessageBox msgbox;
		msgbox.setText("Failed trying to remove bad files." + sql.lastError().text());
		msgbox.exec();
	}

	bool r = sql.first();
	while (r) {
		if (!QFile::remove(sql.value(0).toString())) {
			QMessageBox msgbox;
			msgbox.setText("Failed to remove file " + sql.value(0).toString());
			msgbox.exec();
		}
		QSqlQuery sqlrem;
//		sqlrem.prepare("DELETE FROM downloads WHERE download_id = ?");
		sqlrem.prepare("UPDATE downloads SET dldownloaded = false, parsing_failed=false WHERE download_id = ?");
		sqlrem.bindValue(0, sql.value(1));
		if (!sqlrem.exec()) {
			QMessageBox msgbox;
			msgbox.setText("Failed trying to remove download_id " + sql.value(1).toString() + ".");
			msgbox.exec();
		}
		r = sql.next();
	}
	refreshLists();
}

MainWindow::~MainWindow()
{
	delete ui;
}
