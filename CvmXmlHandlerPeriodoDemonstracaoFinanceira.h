#ifndef CVMXMLHANDLERPERIODODEMONSTRACAOFINANCEIRA_H
#define CVMXMLHANDLERPERIODODEMONSTRACAOFINANCEIRA_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerPeriodoDemonstracaoFinanceira : public CvmXmlHandler
{
public:
	CvmXmlHandlerPeriodoDemonstracaoFinanceira(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERPERIODODEMONSTRACAOFINANCEIRA_H
