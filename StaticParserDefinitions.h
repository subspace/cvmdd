#ifndef STATICPARSERDATA_H
#define STATICPARSERDATA_H

#include "ParserDefinitions.h"

namespace StaticParserDefinitions {

#define CVM_VERSION_UNKNOWN		0X0000
#define CVM_VERSION_2			0x0001
#define CVM_VERSION_3			0x0002
#define CVM_VERSION_39			0x0004
#define CVM_VERSION_4			0x0008
#define CVM_VERSION_5			0x0010
#define CVM_VERSION_6			0x0020
#define CVM_VERSION_7			0x0040
#define CVM_VERSION_8			0x0080
#define CVM_VERSION_9			0x0100
#define CVM_VERSION_91			0x0200
#define CVM_VERSION_92			0x0400
#define CVM_VERSION_ENET_ITR	0x0800 //NEW!!!
#define CVM_VERSION_ENET_DFP	0x1000 //NEW!!!
#define CVM_VERSION_ALL			0x1fff

extern QStringList fileList;
extern QStringList newVersionFileList;

ParserDefinitions *createParserDefinitions(const QString &fileName, int version);
void initParserDefinitions();
void deleteParserDefinitions();
bool fileNameExists(const QString &fileName);
ParserDefinitions *getParserDefinition(const QString &fileName, int version = CVM_VERSION_92);

}
#endif // STATICPARSERDATA_H
