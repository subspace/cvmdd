#include "CvmApplication.h"
#include "CvmWebServiceXmlRequestor.h"
#include "CvmWebServiceXmlRequestParser.h"
#include "CvmDownloader.h"
#include "CvmParser.h"
#include "CvmParserThreadStarter.h"
#include "StaticParserDefinitions.h"
#include <QDir>
#include <QSqlDatabase>
#include <QSettings>
#include <QSqlQuery>
#include <QSqlError>
#include <QSemaphore>
#include <QHash>
#include <QDebug>

CvmApplication::CvmApplication(int &argc, char **argv): QApplication(argc, argv)
{
	connect(this, SIGNAL(aboutToQuit()), SLOT(cleanup()));
}

bool CvmApplication::init()
{
	QCoreApplication::setOrganizationName("begin_systems");
	QCoreApplication::setOrganizationDomain("beginsystems.com.br");
	QCoreApplication::setApplicationName("cvmdd");

	saveSettings();

	StaticParserDefinitions::initParserDefinitions();
	createDefaultDirectories();
	networkManager = new QNetworkAccessManager(this);

	return openDataBaseConnections();
}

bool CvmApplication::openDataBaseConnections()
{
	QSettings		set;
	QSqlDatabase	db;

	connectionPool.acquire(connectionPool.available());
	connectionPool.release(set.value("db_max_connections").toUInt());

	db = QSqlDatabase::addDatabase(set.value("db_type").toString(), QSqlDatabase::defaultConnection);
	db.setHostName(set.value("db_address").toString());
	db.setPort(set.value("db_port").toInt());
	db.setDatabaseName(set.value("db_name").toString());
	db.setUserName(set.value("db_username").toString());
	db.setPassword(set.value("db_password").toString());
	if (!db.open())
		return false;

	return true;
}

void CvmApplication::requestXMLs(const QDate &startDate, const QDate &endDate, CvmWebServiceXmlRequestor::DocType docType)
{
	QSettings set;
	QDate date = startDate;

	while (date <= endDate) {
		CvmWebServiceXmlRequestor *req = new CvmWebServiceXmlRequestor(networkManager, this);
		req->setDocType(docType);
		req->setUsername(set.value("cvm_username").toString());
		req->setPassword(set.value("cvm_password").toString());
		req->setDate(date);
		req->setTime(QTime(00, 00));
		if (!req->request()) {
			qDebug() << "call" << date;
			req->deleteLater();
			return;
		}
		connect(req, SIGNAL(progress(CvmWebServiceXmlRequestor*,qint64,qint64)), this, SLOT(requestDownloadProgress(CvmWebServiceXmlRequestor*,qint64,qint64)));
		connect(req, SIGNAL(finished(CvmWebServiceXmlRequestor*,QByteArray)), this, SLOT(requestFinished(CvmWebServiceXmlRequestor*,QByteArray)));
		connect(req, SIGNAL(error(CvmWebServiceXmlRequestor*,QNetworkReply::NetworkError)), this, SLOT(requestError(CvmWebServiceXmlRequestor*,QNetworkReply::NetworkError)));
		emit progressUpdate(qHash(date.toString()), "XML from date " + date.toString("dd/MM/yyyy"), "Requested.");
		date = date.addDays(1);
	}
}

void CvmApplication::requestDownloadProgress(CvmWebServiceXmlRequestor *request, qint64 bytesReceived, qint64 bytesTotal)
{
	unsigned int requestID = qHash(request->getDate().toString());

	QString requestDesc = "XML from date " + request->getDate().toString("dd/MM/yyyy");
	emit progressUpdate(requestID, requestDesc, "Download " + QString::number((float)bytesReceived/bytesTotal*100, 'f', 2) + "%");
}

void CvmApplication::parseDownloadedFiles()
{
	QSqlQuery sel;

	sel.prepare("SELECT download_id, dlfilepath FROM downloads WHERE dldownloaded = true AND dlparsed = false");
//	sel.prepare("SELECT download_id, dlfilepath FROM downloads WHERE download_ID = 95");
	if (!sel.exec()) {
		qDebug() << sel.lastError().text();
		return;
	}

	bool r = sel.first();
	while (r) {
		CvmParserThreadStarter *parserThread = new CvmParserThreadStarter(&connectionPool, sel.value(1).toString(), sel.value(0).toUInt(), this);
		connect(parserThread, SIGNAL(parsingTerminated(CvmParser*)), this, SLOT(parsingTerminated(CvmParser*)));
		connect(parserThread, SIGNAL(parsingFinished(CvmParser*,bool)), this, SLOT(parsingFinished(CvmParser*,bool)));
		parserThread->start();

		emit progressUpdate(parserThread->getDownloadID(), parserThread->getFileName(), "Waiting...");
		qDebug() << "wtfff";
		r = sel.next();
	}
}

void CvmApplication::parsingTerminated(CvmParser *parser)
{
	qDebug() << "terminated." << parser->getDatabase().connectionName();
	emit progressUpdate(parser->getDownloadID(), parser->getFilePath(), "Error: Terminated!");

	delete parser;
}

void CvmApplication::parsingFinished(CvmParser *parser, bool success)
{
	if (success) {
		QSqlQuery sql;

		sql.prepare("UPDATE downloads SET dlparsed = true, parsing_failed = false WHERE download_id = ?");
		sql.bindValue(0, parser->getDownloadID());
		if (!sql.exec()) {
			emit progressUpdate(parser->getDownloadID(), parser->getFilePath(), sql.lastError().text());
			qDebug() << parser->getDownloadID() << " failed!" << parser->getDatabase().connectionName();
			return;
		}
		emit progressUpdate(parser->getDownloadID(), parser->getFilePath(), "Finished!");
		qDebug() << parser->getDownloadID() << " succeeded!" << parser->getDatabase().connectionName();
		QFile::remove(parser->getFilePath());
		sql.finish();
	} else {
		emit progressUpdate(parser->getDownloadID(), parser->getFilePath(), "Failed!");
		QSqlQuery sql;

		sql.prepare("UPDATE downloads SET parsing_failed = true WHERE download_id = ?");
		sql.bindValue(0, parser->getDownloadID());
		sql.exec();

		qDebug() << parser->getDownloadID() << " failed!" << parser->getDatabase().connectionName();
		sql.finish();
	}

	delete parser;
}

bool CvmApplication::parseCleanup()
{
	QSqlQuery		sql;
	QSqlDatabase	db = QSqlDatabase::database();

	sql.prepare("SELECT download_id FROM downloads");
	sql.exec();
	bool r = sql.first();
	while (r) {
		if (!CvmParser::cleanupSQLDatabase(db, sql.value(0).toUInt())) {
			qDebug() << "failed cleanup";
			return false;
		}
		r = sql.next();
	}
	return true;
}

void CvmApplication::downloadPendingFiles()
{
	QSqlQuery select;
	QSettings set;

	select.prepare("SELECT download_id, dlurl FROM downloads WHERE dldownloaded = ?");
	select.bindValue(0, false);
	if (!select.exec()) {
		qDebug() << select.lastError().text() << "download failed.";
		return;
	}
	bool r = select.first();
	while (r) {
		CvmDownloader *d = new CvmDownloader(select.value(0).toUInt(), networkManager, select.value(1).toUrl(), QDir(set.value("data_directory").toString()), QString::number(select.value(0).toUInt()), false, this);
		connect(d, SIGNAL(finished(CvmDownloader*)), this, SLOT(downloadFinished(CvmDownloader*)));
		connect(d, SIGNAL(error(CvmDownloader*,QNetworkReply::NetworkError)), this, SLOT(downloadError(CvmDownloader*,QNetworkReply::NetworkError)));
		connect(d, SIGNAL(progress(CvmDownloader*,qint64,qint64)), this, SLOT(downloadProgress(CvmDownloader*,qint64,qint64)));
		d->startDownload();
		emit progressUpdate(d->getDownloadID(), d->getAbsoluteSaveFileName(), "Waiting!");
		r = select.next();
	}

}

void CvmApplication::downloadProgress(CvmDownloader *downloader, qint64 bytesReceived, qint64 bytesTotal)
{
	emit progressUpdate(downloader->getDownloadID(), downloader->getAbsoluteSaveFileName(), "Download " + QString::number(bytesReceived) + "/" + QString::number(bytesTotal));
}

void CvmApplication::downloadFinished(CvmDownloader *downloader)
{
	QSqlQuery upd;

	upd.prepare("UPDATE downloads SET dldownloaded = true, dlfilepath = ? WHERE download_id = ?");
	upd.bindValue(0, downloader->getAbsoluteSaveFileName());
	upd.bindValue(1, downloader->getDownloadID());
	if (!upd.exec()) {
		qDebug() << "Failed to mark download as finished." << upd.lastError().text();
	}
	qDebug() << downloader->getDownloadID() << "finished...";

	downloader->deleteLater();
	emit progressUpdate(downloader->getDownloadID(), downloader->getAbsoluteSaveFileName(), "Finished!");
}

void CvmApplication::downloadError(CvmDownloader *downloader, QNetworkReply::NetworkError error)
{
	qDebug() << downloader->getDownloadID() << error << "error";
	downloader->deleteLater();
	emit progressUpdate(downloader->getDownloadID(), downloader->getAbsoluteSaveFileName(), "Error " + QString::number(error));
}

void CvmApplication::requestFinished(CvmWebServiceXmlRequestor *request, const QByteArray &xmlResponse)
{
	unsigned int	requestID = qHash(request->getDate().toString());
	QString			requestDesc = "XML from date " + request->getDate().toString("dd/MM/yyyy");

	request->deleteLater();

	emit progressUpdate(requestID, requestDesc, "Parsing...");

	CvmWebServiceXmlRequestParser parser(xmlResponse);
	if (!parser.parse()) {
		emit progressUpdate(requestID, requestDesc, parser.getError());
		return;
	}

	QList<CvmWebServiceXmlRequestParser::CvmDocInfo> docs = parser.getParsedDocuments();
	for (int i = 0; i < docs.size(); i++) {
		if (docs[i].docType == CvmWebServiceXmlRequestor::ITR || docs[i].docType == CvmWebServiceXmlRequestor::DFP || docs[i].docType == CvmWebServiceXmlRequestor::IAN /*|| docs[i].docType == CvmWebServiceXmlRequestor::FRE || docs[i].docType == CvmWebServiceXmlRequestor::FCA*/) {
			QSqlQuery sql;

			sql.prepare("SELECT download_id, dldatasolicitada FROM downloads WHERE (dlcodcvmid = ? AND dltipodocumento = ? AND dldataref = ?)");
			sql.bindValue(0, docs[i].ccvm);
			sql.bindValue(1, CvmWebServiceXmlRequestor::getDocTypeString(docs[i].docType));
			sql.bindValue(2, docs[i].dataRef);
			if (!sql.exec()) {
				emit progressUpdate(requestID, requestDesc, sql.lastError().text());
//				qDebug() << sql.lastError().text() << "Failed on insertion into the download list. Please requery your xml period.";
				return;
			}
			if (sql.first()) {
				if (sql.value(1).toDateTime() >= parser.getRequestedDateTime()) //nothing to be done
					continue;

				QSqlQuery update;
				update.prepare("UPDATE downloads SET dldatasolicitada = ?, dlurl = ?, dldataconsulta = ?, dldownloaded = 0, dlparsed = 0 WHERE download_id = ?");
				update.bindValue(0, parser.getRequestedDateTime());
				update.bindValue(1, docs[i].url.toString());
				update.bindValue(2, parser.getRequestDateTime());
				update.bindValue(3, sql.value(0));
				if (!update.exec()) {
					emit progressUpdate(requestID, requestDesc, update.lastError().text());
//					qDebug() << update.lastError().text() << "Failed on insertion into the download list. Please requery your xml period.";
					return;
				}
				continue;
			}

			QSqlQuery insert;
			insert.prepare("INSERT INTO downloads (dlcodcvmid, dltipodocumento, dldataref, dlurl, dldownloaded, dlparsed, dldataconsulta, dldatasolicitada) VALUES (?,?,?,?,?,?,?,?)");
			insert.bindValue(0, docs[i].ccvm);
			insert.bindValue(1, CvmWebServiceXmlRequestor::getDocTypeString(docs[i].docType));
			insert.bindValue(2, docs[i].dataRef);
			insert.bindValue(3, docs[i].url.toString());
			insert.bindValue(4, false);
			insert.bindValue(5, false);
			insert.bindValue(6, parser.getRequestDateTime());
			insert.bindValue(7, parser.getRequestedDateTime());
			if (!insert.exec()) {
				emit progressUpdate(requestID, requestDesc, insert.lastError().text());
//				qDebug() << insert.lastError().text() << "Failed on insertion into the download list. Please requery your xml period.";
				return;
			}
		}
	}

	emit progressUpdate(requestID, requestDesc, "Finished!");
}

void CvmApplication::requestError(CvmWebServiceXmlRequestor *request, const QNetworkReply::NetworkError error)
{
	request->deleteLater();
	unsigned int requestID = qHash(request->getDate().toString());
	emit progressUpdate(requestID, "XML from date " + request->getDate().toString("dd/MM/yyyy"), "Network Error " + QString::number(error));
}

void CvmApplication::createDefaultDirectories()
{
	QSettings set;

	QDir *dir = new QDir(set.value("data_directory").toString());
	if (!dir->exists())
		dir->mkdir(dir->path());
}

bool CvmApplication::saveSettings()
{
	QSettings set;

	set.setValue("data_directory", set.value("data_directory", "data_files"));
	set.setValue("db_type", set.value("db_type", "QPSQL"));
	set.setValue("db_address", set.value("db_address", "localhost"));
	set.setValue("db_port", set.value("db_port", 5432));
	set.setValue("db_name", set.value("db_name", "cvmdd"));
	set.setValue("db_username", set.value("db_username", "postgres"));
	set.setValue("db_password", set.value("db_password", "maravilhavirtual"));
	set.setValue("db_max_connections", set.value("db_max_connections", 32));
//	set.setValue("db_max_connections", 10);
	set.setValue("cvm_username", set.value("cvm_username", "397DWLLABUHE"));
	set.setValue("cvm_password", set.value("cvm_password", "doppelganger2"));

	return true;
}

void CvmApplication::cleanup()
{
	StaticParserDefinitions::deleteParserDefinitions();
}

