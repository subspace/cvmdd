#ifndef CVMXMLHANDLERPAGTOPROVENTODINHEIDEMOFINANNEG_H
#define CVMXMLHANDLERPAGTOPROVENTODINHEIDEMOFINANNEG_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg : public CvmXmlHandler
{
public:
	CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERPAGTOPROVENTODINHEIDEMOFINANNEG_H
