#include "Downloader.h"
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

Downloader::Downloader(QNetworkAccessManager *manager, const QUrl &downloadUrl, const QDir &saveDir, const QString &saveFileName, bool saveToMemory, QObject *parent) :
QObject(parent),
manager(manager),
downloadUrl(downloadUrl),
saveDirectory(saveDir),
saveFileName(saveFileName),
saveToMemory(saveToMemory)
{
}

Downloader::~Downloader()
{
}

bool Downloader::startDownload()
{
	downlFinished = false;

	if (saveFileName.isEmpty() && saveToMemory == false)
		return false;

	QNetworkRequest req;
	req.setUrl(downloadUrl);
	reply = manager->get(req);

	connect(reply, SIGNAL(finished()), this, SLOT(downloadFinished()));
	connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(downloadError(QNetworkReply::NetworkError)));
	connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));

	return true;
}

void Downloader::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	emit progress(this, bytesReceived, bytesTotal);
}

void Downloader::cancelDownload()
{
	reply->abort();
	reply->deleteLater();
}

void Downloader::downloadFinished()
{
	if (saveToMemory) {
		fileInMemory = reply->readAll();
	} else {
		qDebug() << saveDirectory.filePath(saveFileName);
		QFile f(saveDirectory.absoluteFilePath(saveFileName));
		f.open(QIODevice::WriteOnly);
		f.write(reply->readAll());
		f.close();
	}
	downlFinished = true;

	emit finished(this);
}

void Downloader::downloadError(QNetworkReply::NetworkError err)
{
	emit error(this, err);
}
