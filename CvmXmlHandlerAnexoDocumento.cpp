#include "CvmXmlHandlerAnexoDocumento.h"

CvmXmlHandlerAnexoDocumento::CvmXmlHandlerAnexoDocumento(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *anexoDocumento = addTableMapping("AnexoDocumento", "tfreanex_docm");

		//mapping only documento tag useful information
		XmlSqlTableMap *documento = addTableMapping("Documento", "tfredocm", anexoDocumento);
		addColumnMapping(documento,			"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
		addColumnMapping(anexoDocumento,	"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true));

	addColumnMapping(anexoDocumento, "NumeroQuadroRelacionado", "num_qdro_relc", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoDocumento, "NumeroCampoRelacionado", "num_camp_relc", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoDocumento, "NumeroGrupoRelacionado", "num_grup_relc", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoDocumento, "NumeroItemTextoLivre", "num_item_txt_livr", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoDocumento, "NumeroSubCampo", "num_sub_camp", ParserDefinitions::SmallInt, true);
	addColumnMapping(anexoDocumento, "TextoCaminhoArquivoOriginal", "txt_cam_arq_orig", ParserDefinitions::VarChar);
	addColumnMapping(anexoDocumento, "NomeArquivoPdf", "nome_arq_pdf", ParserDefinitions::VarChar);
	addColumnMapping(anexoDocumento, "ImagemObjetoArquivoPdf", "imag_objt_arq_pdf", ParserDefinitions::ByteArray);
	addColumnMapping(anexoDocumento, "Idioma", "cod_idio", ParserDefinitions::SmallInt);
}
