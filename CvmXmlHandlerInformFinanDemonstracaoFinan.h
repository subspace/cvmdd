#ifndef CVMXMLHANDLERINFORMFINANDEMONSTRACAOFINAN_H
#define CVMXMLHANDLERINFORMFINANDEMONSTRACAOFINAN_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerInformFinanDemonstracaoFinan : public CvmXmlHandler
{
public:
	CvmXmlHandlerInformFinanDemonstracaoFinan(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERINFORMFINANDEMONSTRACAOFINAN_H
