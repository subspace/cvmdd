#ifndef PARSERDEFINITIONS_H
#define PARSERDEFINITIONS_H

#include <QList>
#include <QString>

class ParserFixup;
class ParserDefinitions
{
public:
	//those types are imported from mssqlserver... the first database type of this system... so that won't be changed soon, those types are used just to identify the field type.
	enum FieldType { None = 0, Text = 35, TinyInt = 48, SmallInt = 52, Int = 56, SmallDateTime = 58, Real = 59, Money = 60, DateTime = 61, Float = 62, NText = 99, Bit = 104, Decimal = 106, Numeric = 108, SmallMoney = 122, BigInt = 127, VarChar = 167, Char = 175, NVarChar = 231, NChar = 239, Time = 240, ByteArray = 241};

private:
	QString				parserName;

public:
	void				setName(const QString &name) { this->parserName = name; }
	const QString		&name() const { return this->parserName; }
	int					size() const { return fields.size(); }
	const QString		&fieldName(quint32 fieldPos) { return fields[fieldPos]->name; }
	quint32				fieldSize(quint32 fieldPos) { return fields[fieldPos]->size; }
	FieldType			fieldType(quint32 fieldPos) { return fields[fieldPos]->type; }

	//Remember this object doesn't take ownership of fixups, you must remember to delete them after use
	void				setFixup(ParserFixup *fixup) { fix = fixup; }
	ParserFixup			*fixup() { return fix; }
	void				setFieldFixup(int field, ParserFixup *fixup) { fields[field]->fix = fixup; }
	ParserFixup			*fieldFixup(int field) { return fields[field]->fix; }

	bool				setFormat(int field, const QString &dateFormat);
	const QString		&getFormat(int field) const { return this->fields[field]->format; }

private:
	struct ParserField {
		QString		name;
		quint32		size;
		FieldType	type;
		QString		format;
		ParserFixup *fix;
	};
	QList<ParserField *> fields;

	ParserField			*findField(const QString &name) const;
	ParserFixup			*fix;

public:
	ParserDefinitions(const QString &name):parserName(name), fix(NULL) {}
	virtual ~ParserDefinitions();
public:
	bool				addField(const QString &name, quint32 size, FieldType type = Char, ParserFixup *fix = NULL);
	bool				removeField(const QString &name);

};

#endif // PARSERDEFINITIONS_H
