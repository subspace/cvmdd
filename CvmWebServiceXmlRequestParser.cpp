#include "CvmWebServiceXmlRequestParser.h"
#include <QtXml/QXmlInputSource>
#include <QDebug>

CvmWebServiceXmlRequestParser::CvmWebServiceXmlRequestParser(const QByteArray &xmlData): xmlData(xmlData)
{
}

bool CvmWebServiceXmlRequestParser::parse()
{
	documents.clear();

	this->xmlData = xmlData;
	xmlDataBuffer.setData(xmlData);
	QXmlInputSource inputSource(&this->xmlDataBuffer);

	QXmlSimpleReader reader;

	cvmError = false;
	reader.setContentHandler(this);
	return reader.parse(inputSource);
}

bool CvmWebServiceXmlRequestParser::startDocument()
{
	return true;
}

bool CvmWebServiceXmlRequestParser::startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts)
{
	Q_UNUSED(namespaceURI)
	Q_UNUSED(qName)

	if (localName == "ERROS") {
		cvmError = true;
		requestedDateTime = QDateTime::fromString(atts.value("DataSolicitada"), "dd/MM/yyyy hh:mm");
		requestDateTime = QDateTime::fromString(atts.value("DataConsulta"), "dd/MM/yyyy hh:mm");
		requestedDocType = CvmWebServiceXmlRequestor::getDocTypeFromString(atts.value("TipoDocumento"));

		return true;
	}
	if (cvmError) {
		if (localName == "NUMERO_DO_ERRO") {
			currentText.clear();
			return true;
		}
		if (localName == "DESCRICAO_DO_ERRO") {
			currentText.clear();
			return true;
		}
		if (localName == "FONTE_DO_ERRO") {
			currentText.clear();
			return true;
		}
	}

	if (localName == "Link") {
		CvmDocInfo doc;

		doc.url = QUrl(atts.value("url"));
		doc.docType = CvmWebServiceXmlRequestor::getDocTypeFromString(atts.value("Documento"));

		if (doc.docType == CvmWebServiceXmlRequestor::IPE) {
			doc.especie = atts.value("Especie");
			doc.tipo = atts.value("Tipo");
			doc.categoria = atts.value("Categoria");
		}

		doc.ccvm = atts.value("ccvm").toUInt();

		//FrmDtRef is messed up so try common masks instead, cvm always with buggy systems lol.
		doc.dataRef = QDateTime::fromString(atts.value("DataRef"), "dd/MM/yyyy hh:mm:ss");
		if (!doc.dataRef.isValid())
			doc.dataRef = QDateTime::fromString(atts.value("DataRef"), "dd/MM/yyyy hh:mm");
		if (!doc.dataRef.isValid())
			doc.dataRef = QDateTime::fromString(atts.value("DataRef"), "dd/MM/yyyy");

		doc.situacao = atts.value("Situacao");

		documents.push_back(doc);
	}
	return true;
}

bool CvmWebServiceXmlRequestParser::endElement(const QString &namespaceURI, const QString &localName, const QString &qName)
{
	Q_UNUSED(namespaceURI)
	Q_UNUSED(qName)
	if (localName == "NUMERO_DO_ERRO" || localName == "DESCRICAO_DO_ERRO" || localName == "FONTE_DO_ERRO") {
		error += currentText + " - ";
		return true;
	}

	return true;
}

bool CvmWebServiceXmlRequestParser::endDocument()
{
	if (error.isEmpty())
		return true;

	error.chop(3);
	return false;
}

bool CvmWebServiceXmlRequestParser::characters(const QString &ch)
{
	currentText += ch;
	return true;
}
