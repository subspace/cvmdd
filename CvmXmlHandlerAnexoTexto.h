#ifndef CVMXMLHANDLERANEXOTEXTO_H
#define CVMXMLHANDLERANEXOTEXTO_H

#include "CvmXmlHandler.h"

class CvmXmlHandlerAnexoTexto : public CvmXmlHandler
{
public:
	CvmXmlHandlerAnexoTexto(QSqlDatabase &db, quint32 downloadID);
};

#endif // CVMXMLHANDLERANEXOTEXTO_H
