#-------------------------------------------------
#
# Project created by QtCreator 2012-03-05T14:17:34
#
#-------------------------------------------------

QT       += core sql network xml gui

TARGET = cvmdd

TEMPLATE = app

LIBS += /lib/libquazip.so

INCLUDEPATH += /include

SOURCES += main.cpp \
    Parser.cpp \
    ParserDefinitions.cpp \
    StaticParserDefinitions.cpp \
    CvmParser.cpp \
    CvmApplication.cpp \
    ParserFixup.cpp \
    CvmIanHeaderFixup.cpp \
    CvmItrHeaderFixup.cpp \
    CvmCTRHoraEmissFixup.cpp \
    Downloader.cpp \
    CvmDownloader.cpp \
    CvmParserThreadStarter.cpp \
    MainWindow.cpp \
    DialogConfig.cpp \
    DialogAbout.cpp \
    DialogProgress.cpp \
    CvmXmlHandlerDocumento.cpp \
    CvmXmlHandler.cpp \
    CvmXmlHandlerAnexoDocumento.cpp \
    CvmXmlHandlerAnexoTexto.cpp \
    CvmXmlHandlerCompCapitSocDemonsFinanNegocios.cpp \
    CvmXmlHandlerFormDemonstracaoFinanceiraITR.cpp \
    CvmXmlHandlerInfoFinaDfin.cpp \
    CvmXmlHandlerInformFinanDemonstracaoFinan.cpp \
    CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg.cpp \
    CvmXmlHandlerPeriodoDemonstracaoFinanceira.cpp \
    CvmXmlHandlerResumInfoFinanDemoFinan.cpp \
    CvmWebServiceXmlRequestor.cpp \
    CvmWebServiceXmlRequestParser.cpp

HEADERS += \
    Parser.h \
    ParserDefinitions.h \
    StaticParserDefinitions.h \
    CvmParser.h \
    CvmApplication.h \
    ParserFixup.h \
    CvmIanHeaderFixup.h \
    CvmItrHeaderFixup.h \
    CvmCTRHoraEmissFixup.h \
    Downloader.h \
    CvmDownloader.h \
    CvmParserThreadStarter.h \
    MainWindow.h \
    DialogConfig.h \
    DialogAbout.h \
    DialogProgress.h \
    CvmXmlHandlerDocumento.h \
    CvmXmlHandler.h \
    CvmXmlHandlerAnexoDocumento.h \
    CvmXmlHandlerAnexoTexto.h \
    CvmXmlHandlerCompCapitSocDemonsFinanNegocios.h \
    CvmXmlHandlerFormDemonstracaoFinanceiraITR.h \
    CvmXmlHandlerInfoFinaDfin.h \
    CvmXmlHandlerInformFinanDemonstracaoFinan.h \
    CvmXmlHandlerPagtoProventoDinheiDemoFinanNeg.h \
    CvmXmlHandlerPeriodoDemonstracaoFinanceira.h \
    CvmXmlHandlerResumInfoFinanDemoFinan.h \
    CvmWebServiceXmlRequestor.h \
    CvmWebServiceXmlRequestParser.h

OTHER_FILES +=

FORMS += \
    MainWindow.ui \
    DialogConfig.ui \
    DialogAbout.ui \
    DialogProgress.ui

RESOURCES += \
    resources.qrc
