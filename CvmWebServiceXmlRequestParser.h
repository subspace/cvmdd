#ifndef CVMXMLPARSER_H
#define CVMXMLPARSER_H

#include "CvmWebServiceXmlRequestor.h"
#include <QBuffer>
#include <QXmlDefaultHandler>
#include <QDebug>

class QBuffer;

class CvmWebServiceXmlRequestParser: public QXmlDefaultHandler
{
public:
	struct CvmDocInfo {
		QUrl url;
		CvmWebServiceXmlRequestor::DocType docType;
		quint32 ccvm;
		QDateTime dataRef;
		QString situacao;

		//IPE only information
		QString categoria;
		QString tipo;
		QString especie;
	};

private:
	QDateTime	requestedDateTime;
	CvmWebServiceXmlRequestor::DocType requestedDocType;
	QDateTime	requestDateTime;
	QString		error;

public:
	const QDateTime &getRequestedDateTime() const { return this->requestDateTime; }
	const QDateTime &getRequestDateTime() const { return this->requestDateTime; }
	CvmWebServiceXmlRequestor::DocType getRequestedDocType() const { return requestedDocType; }
	const QList<CvmDocInfo> &getParsedDocuments() const { return this->documents; }
	const QString &getError() const { return this->error; }

private:
	QBuffer		xmlDataBuffer;
	QByteArray	xmlData;
	bool		cvmError;
	QList<CvmDocInfo> documents;
	QString		currentText;

	bool startDocument();
	bool startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts);
	bool endElement(const QString &namespaceURI, const QString &localName, const QString &qName);
	bool endDocument();
	bool characters(const QString &ch);

public:
	CvmWebServiceXmlRequestParser(const QByteArray &xmlData);
	~CvmWebServiceXmlRequestParser() {}
public:
	bool parse();

};

#endif // CVMXMLPARSER_H
