#ifndef CVMAPPLICATION_H
#define CVMAPPLICATION_H

#include <QApplication>
#include <QSqlDatabase>
#include <QSemaphore>
#include <QNetworkReply>
#include "CvmWebServiceXmlRequestor.h"

class QSqlDatabase;
class QDate;
class QNetworkAccessManager;
class CvmDownloader;
class CvmParser;

class CvmApplication: public QApplication
{
	Q_OBJECT
public:
private:
public:

private:
	QSemaphore					connectionPool;
	QNetworkAccessManager		*networkManager;

	bool openDataBaseConnections();
	void createDefaultDirectories();

private slots:
	void cleanup();

	//XML request
	void requestDownloadProgress(CvmWebServiceXmlRequestor *request, qint64 bytesReceived, qint64 bytesTotal);
	void requestFinished(CvmWebServiceXmlRequestor *request, const QByteArray &xmlResponse);
	void requestError(CvmWebServiceXmlRequestor *request, const QNetworkReply::NetworkError error);

	//CVM File download
	void downloadFinished(CvmDownloader *downloader);
	void downloadError(CvmDownloader *downloader, QNetworkReply::NetworkError error);
	void downloadProgress(CvmDownloader *downloader, qint64 bytesReceived, qint64 bytesTotal);

	//Parser
	void parsingFinished(CvmParser *parser, bool success);
	void parsingTerminated(CvmParser *parser);

public:
	CvmApplication(int &argc, char **argv);
	bool init();
	bool saveSettings();

public slots:
	void requestXMLs(const QDate &startDate, const QDate &endDate, CvmWebServiceXmlRequestor::DocType docType);
	void downloadPendingFiles();
	void parseDownloadedFiles();
	bool parseCleanup();

signals:
	void progressUpdate(unsigned int id, const QString &desc, const QString &status);

};

#endif // CVMAPPLICATION_H
