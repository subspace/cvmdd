#ifndef DIALOGPROGRESS_H
#define DIALOGPROGRESS_H

#include <QDialog>
#include <QMutex>

namespace Ui {
	class DialogProgress;
}

class QStandardItemModel;

class DialogProgress : public QDialog
{
	Q_OBJECT
	
public:
	explicit DialogProgress(QWidget *parent = 0);
	~DialogProgress();
	void setActiveCheckFunction(int (*check)()) { this->getActiveCount = check; }

public slots:
	void updateProgress(unsigned int id, const QString &desc, const QString &status);

protected:
	void closeEvent(QCloseEvent *evt);

private:
	QMutex				mutex;
	Ui::DialogProgress	*ui;
	QStandardItemModel	*model;
	QTimer				*timer;
	int (*getActiveCount)();

private slots:
	void on_timer_timeout();
};

#endif // DIALOGPROGRESS_H
