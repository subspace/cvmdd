#include "CvmXmlHandlerInfoFinaDfin.h"

CvmXmlHandlerInfoFinaDfin::CvmXmlHandlerInfoFinaDfin(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *infoFinaDfin = addTableMapping("InfoFinaDfin", "tfreinfo_fina_dfin");
	addColumnMapping(infoFinaDfin, "NumeroIdentificadorInfoFinaDFin", "num_idt_info_fina_dfin", ParserDefinitions::Int, true);
		//periodo demonstracao financeira
		XmlSqlTableMap *perdfin = addTableMapping("PeriodoDemonstracaoFinanceira", "tfreperi_dfin", infoFinaDfin);
			XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin", perdfin);
				XmlSqlTableMap *documento = addTableMapping("Documento",	"tfredocm", form);
				addColumnMapping(documento,		"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
				addColumnMapping(form,			"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
				addColumnMapping(perdfin,		"NumeroSequencialDocumento",	"num_seq_docm",		 ParserDefinitions::Int, true,
				addColumnMapping(infoFinaDfin,	"NumeroSequencialDocumento", "num_seq_docm",		 ParserDefinitions::Int, true))));
				addColumnMapping(documento,		"CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false,
//				addColumnMapping(form,			"CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false,
//				addColumnMapping(perdfin,		"CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false,
				addColumnMapping(infoFinaDfin,	"CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt, false));
					XmlSqlTableMap *comp = addTableMapping("CompanhiaAberta", "tfrecoab", documento);
					addColumnMapping(comp, "TipoEmpresa", "cod_tipo_emp", ParserDefinitions::SmallInt, false,
					addColumnMapping(form, "TipoEmpresa", "cod_tipo_emp", ParserDefinitions::SmallInt, false,
					addColumnMapping(infoFinaDfin, "TipoEmpresa", "cod_tipo_emp", ParserDefinitions::SmallInt, false)));

		addColumnMapping(perdfin,	"NumeroIdentificacaoPeriodo", "num_idt_peri_dfin", ParserDefinitions::SmallInt, true); //fail

		XmlSqlTableMap *planoConta = addTableMapping("PlanoConta", "tfrepcon", infoFinaDfin);
			XmlSqlTableMap *versaoPlanoConta = addTableMapping("VersaoPlanoConta", "tfrevers_pcon", planoConta);
//			addColumnMapping(versaoPlanoConta,	"CodigoTipoDocumento",				"cod_tipo_docm",		ParserDefinitions::SmallInt, true,
//				addColumnMapping(planoConta,	"CodigoTipoDocumento",				"cod_tipo_docm",		ParserDefinitions::SmallInt, true,
//				addColumnMapping(infoFinaDfin,	"CodigoTipoDocumento",				"cod_tipo_docm",		ParserDefinitions::SmallInt, false))); //fail fixed
//			addColumnMapping(versaoPlanoConta,	"CodigoTipoEmpresa",				"cod_tipo_emp",			ParserDefinitions::SmallInt, true,
//				addColumnMapping(planoConta,	"CodigoTipoEmpresa",				"cod_tipo_emp",			ParserDefinitions::SmallInt, true,
//				addColumnMapping(infoFinaDfin,	"CodigoTipoEmpresa",				"cod_tipo_emp",			ParserDefinitions::SmallInt, false))); //fail fixed
			addColumnMapping(versaoPlanoConta,	"CodigoTipoDemonstracaoFinanceira",	"cod_tipo_dfin",		ParserDefinitions::SmallInt, true,
				addColumnMapping(planoConta,	"CodigoTipoDemonstracaoFinanceira", "cod_tipo_dfin",		ParserDefinitions::SmallInt, true,
				addColumnMapping(infoFinaDfin,	"CodigoTipoDemonstracaoFInanceira", "cod_tipo_dfin",		ParserDefinitions::SmallInt, false)));
			addColumnMapping(versaoPlanoConta,	"CodigoTipoInformacaoFinanceira",	"cod_tipo_info_fina",	ParserDefinitions::SmallInt, true,
				addColumnMapping(planoConta,	"CodigoTipoInformacaoFinanceira",	"cod_tipo_info_fina",	ParserDefinitions::SmallInt, true,
				addColumnMapping(infoFinaDfin,	"CodigoTipoInformacaoFinanceira",	"cod_tipo_info_fina",	ParserDefinitions::SmallInt, false)));
			addColumnMapping(versaoPlanoConta,	"NumeroVersaoPlanoConta",			"num_vers_pcon",		ParserDefinitions::SmallInt, true,
				addColumnMapping(planoConta,	"NumeroVersaoPlanoConta",			"num_vers_pcon",		ParserDefinitions::SmallInt, true,
				addColumnMapping(infoFinaDfin,	"NumeroVersaoPlanoConta",			"num_vers_pcon",		ParserDefinitions::SmallInt, false)));
//			addColumnMapping(versaoPlanoConta,	"IndicadorVersaoAtiva",				"ind_vers_atva",		ParserDefinitions::Bit);
		addColumnMapping(planoConta,  "NumeroConta",				"num_con",		ParserDefinitions::VarChar, true,
		addColumnMapping(infoFinaDfin,"NumeroConta",				"num_con",		ParserDefinitions::VarChar, false));
		addColumnMapping(planoConta, "IndicadorContaFixa",		"ind_con_fix",	ParserDefinitions::Bit);
		addColumnMapping(planoConta, "NumeroOrdemConta",		"num_orde_con", ParserDefinitions::SmallInt);
		addColumnMapping(planoConta, "IndicadorQuantidade",		"ind_qte",		ParserDefinitions::Bit);
		addColumnMapping(planoConta, "IndicadorDecimal",		"ind_deci",		ParserDefinitions::Bit);
		addColumnMapping(planoConta, "IndicadorValidacaoNivel", "ind_vald_niv", ParserDefinitions::Bit);
		addColumnMapping(planoConta, "Formulas");

	addColumnMapping(infoFinaDfin, "DescricaoConta1",	"desc_con1", ParserDefinitions::VarChar);
	addColumnMapping(infoFinaDfin, "DescricaoConta2",	"desc_con2", ParserDefinitions::VarChar);
	addColumnMapping(infoFinaDfin, "DescricaoConta3",	"desc_con3", ParserDefinitions::VarChar);
	addColumnMapping(infoFinaDfin, "ValorConta1",		"val_con1",	 ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta2",		"val_con2",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta3",		"val_con3",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta4",		"val_con4",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta5",		"val_con5",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta6",		"val_con6",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta7",		"val_con7",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta8",		"val_con8",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta9",		"val_con9",  ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta10",		"val_con10", ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta11",		"val_con11", ParserDefinitions::Numeric);
	addColumnMapping(infoFinaDfin, "ValorConta12",		"val_con12", ParserDefinitions::Numeric);
}
