#include "CvmXmlHandlerDocumento.h"

CvmXmlHandlerDocumento::CvmXmlHandlerDocumento(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *docTable = addTableMapping("Documento", "tfredocm");
	addColumnMapping(docTable, "Documentos");

	XmlSqlTableMap *versLite = addTableMapping("VersaoLiteral", "tfrevers_lite", docTable);
		addColumnMapping(versLite, "NumeroVersaoLiteral", "num_vers_lite", ParserDefinitions::SmallInt, true,
		addColumnMapping(docTable, "NumeroVersaoLiteral", "num_vers_lite", ParserDefinitions::SmallInt));
		addColumnMapping(versLite, "IndicadorVersaoLiteralAtivo", "ind_vers_lite_atva", ParserDefinitions::Bit);

	XmlSqlTableMap *versExecSist = addTableMapping("VersaoExecutavelSistema", "tfrevers_exec_sist", docTable);
		addColumnMapping(versExecSist,	"NumeroVersaoExecutavelSistema", "num_vers_exec_sist", ParserDefinitions::SmallInt, true,
		addColumnMapping(docTable,		"NumeroVersaoExecutavelSistema", "num_vers_exec_sist", ParserDefinitions::SmallInt));
		addColumnMapping(versExecSist,	"NumeroVersaoBancoDadosSistema", "num_vers_bd_sist", ParserDefinitions::SmallInt, true,
		addColumnMapping(docTable,		"NumeroVersaoBancoDadosSistema", "num_vers_bd_sist", ParserDefinitions::SmallInt));
		addColumnMapping(versExecSist,	"DataInicioValidadeVersao", "data_inic_vald_vers", ParserDefinitions::DateTime);
		addColumnMapping(versExecSist,	"DataFimValidadeVersao", "data_fim_vald_vers", ParserDefinitions::DateTime);

	XmlSqlTableMap *compAberta = addTableMapping("CompanhiaAberta", "tfrecoab", docTable);
		addColumnMapping(compAberta, "CodigoTipoParticipante", "", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "CodigoCvm", "cod_cvm", ParserDefinitions::NVarChar);
		addColumnMapping(compAberta, "NumeroAnoEncerramentoExercicioSocial", "num_ano_encr_exso", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta,	"NumeroSequencialRegistroCvm", "num_seq_reg_cvm", ParserDefinitions::Int, true,
		addColumnMapping(docTable,		"NumeroSequencialRegistroCvm", "num_seq_reg_cvm", ParserDefinitions::Int));
		addColumnMapping(compAberta, "IndicadorSociedadeAnonimaAtiva", "ind_soc_anon_ativ", ParserDefinitions::Bit);
		addColumnMapping(compAberta, "NomeRazaoSocialCompanhiaAberta", "nome_rsoc_coab", ParserDefinitions::VarChar);
		addColumnMapping(compAberta, "NumeroCnpjCompanhiaAberta", "num_cnpj_coab", ParserDefinitions::VarChar);
		addColumnMapping(compAberta, "CodigoCategoriaEmissor", "cod_catg_emss", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "CodigoSituacaoRegistroCvm", "cod_situ_reg_cvm", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "CodigoSituacaoCompanhiaAberta", "cod_situ_coab", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "NumeroMesEncerramentoExercicioSocial", "num_mes_encr_exso", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "TipoEmpresa", "cod_tipo_emp", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "DataRegistroCvm", "data_reg_cvm", ParserDefinitions::DateTime);
		addColumnMapping(compAberta, "DataInicioRazaoSocial", "data_inic_raz_socl", ParserDefinitions::DateTime);
		addColumnMapping(compAberta, "DataConstituicaoEmpresa", "data_cnst_emp", ParserDefinitions::DateTime);
		addColumnMapping(compAberta, "DataInicioSituacaoCompanhiaAberta", "data_inic_situ_coab", ParserDefinitions::DateTime);
		addColumnMapping(compAberta, "DataInicioSituacaoRegistroCvm", "data_inic_situ_reg_cvm", ParserDefinitions::DateTime);
		addColumnMapping(compAberta, "CodigoSetorAtividadeEmpresa", "cod_seto_atvd_emp", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "CodigoEspecieControleAcionario", "cod_espe_caci", ParserDefinitions::SmallInt);
		addColumnMapping(compAberta, "DataUltimaAlteracaoControleAcionario", "data_ult_alte_caci", ParserDefinitions::DateTime);
		addColumnMapping(compAberta, "DataRegistroAtualCategoriaEmissorCvm", "data_reg_atu_catg_emss_cvm", ParserDefinitions::DateTime);

	XmlSqlColumnMap *numSeqDoc = addColumnMapping(docTable, "NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true);
	addColumnMapping(docTable, "CodigoTipoDocumento", "cod_tipo_docm", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "DataReferenciaDocumento", "data_ref_doc", ParserDefinitions::DateTime);
	addColumnMapping(docTable, "NumeroVersaoDocumento", "num_vers_docm", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "IndicadorPendenciaDocumento", "ind_pend_docm", ParserDefinitions::Bit);
	addColumnMapping(docTable, "IndicadorArquivoValido", "ind_arq_vlid", ParserDefinitions::Bit);
	addColumnMapping(docTable, "CodigoMoeda", "cod_moed", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "CodigoEscalaMoeda", "cod_esca_moed", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "CodigoEscalaQuantidade", "cod_esca_qte", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "DataGeracaoArquivo", "data_gera_arq", ParserDefinitions::DateTime);
	addColumnMapping(docTable, "CodigoTipoApresentacao", "cod_tipo_apre", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "NumeroExigenciaReapresentacaoCvm", "num_exig_rapr_cvm", ParserDefinitions::VarChar);
	addColumnMapping(docTable, "CodigoTipoEntregaDocumento", "cod_tipo_entr_docm", ParserDefinitions::SmallInt);
	addColumnMapping(docTable, "ProtocoloEntrega", "num_prot_entr", ParserDefinitions::VarChar);
	addColumnMapping(docTable, "DataEntrega", "data_entr", ParserDefinitions::DateTime);
	addColumnMapping(docTable, "NomeLoginResponsavelEntrega", "nome_logn_resp_entr", ParserDefinitions::VarChar);
	addColumnMapping(docTable, "DocumentoRelacionado", "num_docm_relc", ParserDefinitions::Int);
	addColumnMapping(docTable, "NumeroVersaoGrupoQuadroCampo", "num_vers_grup_qdro_camp", ParserDefinitions::SmallInt);

	addColumnMapping(docTable, "MotivosReapresentacaoDocumento");
	addColumnMapping(docTable, "MotivosReapresentacaoDocumentoAnteriores");
	XmlSqlTableMap *motivo = addTableMapping("MotivoReapresentacaoDocumento", "tfremotv_rapr_docm", docTable);
		addColumnMapping(motivo, "NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true);
		addColumnMapping(motivo, "VersaoDocumento", "num_vers_ante_docm", ParserDefinitions::SmallInt, true);
		addColumnMapping(motivo, "ProtocoloIpe", "num_prot_ipe_alte_cad", ParserDefinitions::Int);
		addColumnMapping(motivo, "Descricao", "desc_motv_rapr_docm", ParserDefinitions::VarChar);
		addColumnMapping(motivo, "NumeroSequencialMotivoReapresentacao", "num_seq_motv_rapr", ParserDefinitions::Int, true);
		addColumnMapping(motivo, "CodigoIdioma", "cod_idio", ParserDefinitions::SmallInt, true);

	addColumnMapping(docTable, "ControlesDominio");
	XmlSqlTableMap *contrDominio = addTableMapping("ControleDominio", "tfrectrl_vers_tab_domi", docTable);
		addColumnMapping(contrDominio, "NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true);
		addColumnMapping(contrDominio, "CodigoDominio", "cod_tab_domi", ParserDefinitions::SmallInt, true);
		addColumnMapping(contrDominio, "NumeroVersaoDominio", "num_vers_domi", ParserDefinitions::SmallInt);

	addColumnMapping(docTable, "DetalhesDominio");

	XmlSqlTableMap *situArq = addTableMapping("SituacaoArquivo", "tfresitu_arq", docTable);
		addColumnToColumnMapping(numSeqDoc, addColumnMapping(situArq, "NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::SmallInt)); //invisible column lol
		addColumnMapping(situArq, "CodigoSituacaoArquivo", "cod_situ_arq", ParserDefinitions::SmallInt);
		addColumnMapping(situArq, "IndicadorExibicaoArquivoSite", "ind_exib_arq_site", ParserDefinitions::Bit);
			XmlSqlTableMap *inst = addTableMapping("Instituicao", "tfrectrl_gerc_coab", situArq);
				XmlSqlTableMap *regCvm = addTableMapping("RegistroCvm", "tfrereg_cvm", inst);
					addColumnMapping(regCvm,	"NumeroSequencialRegistroCvm", "num_seq_reg_cvm", ParserDefinitions::Int, true,
					addColumnMapping(inst,		"NumeroSequencialRegistroCvm", "num_seq_reg_cvm", ParserDefinitions::Int, true,
					addColumnMapping(situArq,	"NumeroSequencialRegistroCvm", "num_seq_reg_cvm", ParserDefinitions::Int, true)));
					addColumnMapping(regCvm, "CodigoTipoParticipante", "cod_tipo_par", ParserDefinitions::SmallInt);
					addColumnMapping(regCvm, "CodigoCvm", "cod_cvm", ParserDefinitions::VarChar);
					addColumnMapping(regCvm, "IndicadorSociedadeAnonimaAtiva", "ind_sa_ativa", ParserDefinitions::Bit);
				addColumnMapping(inst,		"CodigoInstituicao", "cod_inst", ParserDefinitions::SmallInt, true,
				addColumnMapping(situArq,	"CodigoInstituicao", "cod_inst", ParserDefinitions::SmallInt, true));
				addColumnMapping(inst,		"IndicadorGerenciamentoAtivo", "ind_gerc_ativ", ParserDefinitions::Bit);
		addColumnMapping(situArq, "TextoLogRecebimentoAutomatico", "txt_log_recb_auto", ParserDefinitions::VarChar);

	addColumnMapping(docTable, "HistoricosAcao", "historico_acao", ParserDefinitions::NVarChar);
	addColumnMapping(docTable, "TipoAnalise", "tipo_analise", ParserDefinitions::Bit);
}
