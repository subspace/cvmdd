#include "CvmXmlHandlerFormDemonstracaoFinanceiraITR.h"

CvmXmlHandlerFormDemonstracaoFinanceiraITR::CvmXmlHandlerFormDemonstracaoFinanceiraITR(QSqlDatabase &db, quint32 downloadID): CvmXmlHandler(db, downloadID)
{
	XmlSqlTableMap *form = addTableMapping("FormularioDemonstracaoFinanceira", "tfrefrml_dfin");

	//mapping only documento tag useful information
	XmlSqlTableMap *documento = addTableMapping("Documento", "tfredocm", form);
	addColumnMapping(documento, "NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true,
	addColumnMapping(form,		"NumeroSequencialDocumento", "num_seq_docm", ParserDefinitions::Int, true));
		XmlSqlTableMap *companhia = addTableMapping("CompanhiaAberta", "tfrecoab", documento);
		addColumnMapping(companhia, "NumeroSequencialRegistroCvm", "num_seq_reg_cvm", ParserDefinitions::Int, true);
		addColumnMapping(companhia, "TipoEmpresa", "cod_tipo_emp", ParserDefinitions::SmallInt, false,
		addColumnMapping(form,		"TipoEmpresa", "cod_tipo_emp", ParserDefinitions::SmallInt));

	addColumnMapping(form, "CodigoTipoDemonstrativo", "cod_tipo_dmon", ParserDefinitions::SmallInt);
	addColumnMapping(form, "CodigoTipoRelatorioRevisaoEspecial", "cod_tipo_rela_revi_espc", ParserDefinitions::SmallInt);
	addColumnMapping(form, "CodigoTipoCriterioElaboracao", "cod_tipo_crit_elab", ParserDefinitions::SmallInt);
}
